var curveHalfOrders = map[elliptic.Curve]*big.Int{
		elliptic.P224(): new(big.Int).Rsh(elliptic.P224().Params().N, 1),
		elliptic.P256(): new(big.Int).Rsh(elliptic.P256().Params().N, 1),
		elliptic.P384(): new(big.Int).Rsh(elliptic.P384().Params().N, 1),
		elliptic.P521(): new(big.Int).Rsh(elliptic.P521().Params().N, 1),
}

// N       *big.Int // the order of the base point

func signECDSA(privateKey *ecdsa.PrivateKey, digest []byte) ([]byte, error) {
	r, s, err := ecdsa.Sign(rand.Reader, privateKey, digest)
	if err != nil {
		return nil, err
	}

	s, err = utils.ToLowS(&privateKey.PublicKey, s)
	if err != nil {
		return nil, err
	}

	return utils.MarshalECDSASignature(r, s)
}

// IsLow checks that s is a low-S
func IsLowS(publicKey *ecdsa.PublicKey, s *big.Int) (bool, error) {
	halfOrder, ok := curveHalfOrders[publicKey.Curve]
	if !ok {
		return false, fmt.Errorf("curve not recognized [%s]", publicKey.Curve)
	}

	return s.Cmp(halfOrder) != 1, nil

}

func ToLowS(publicKey *ecdsa.PublicKey, s *big.Int) (*big.Int, error) {
	lowS, err := IsLowS(publicKey, s)
	if err != nil {
		return nil, err
	}

	if !lowS {
		// Set s to N - s that will be then in the lower part of signature space
		// less or equal to half order
		s.Sub(publicKey.Params().N, s)

		return s, nil
	}

	return s, nil
}

