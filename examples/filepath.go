package main

import (
	"fmt"
	"path/filepath"
)

func main() {
	paths := []string{
		"/a/b/c",
		"/b/c",
		"./b/c",
	}

	base := "/a"

	for _, p := range paths {
		rel, err := filepath.Rel(base, p)
		fmt.Printf("%q: %q %v\n", p, rel, err)
	}

	const dirtyPath = "../foo/./bar/../baz.txt"
	cleanPath := filepath.Clean(dirtyPath)
	fmt.Printf("%s -> %s\n", dirtyPath, cleanPath)
}
