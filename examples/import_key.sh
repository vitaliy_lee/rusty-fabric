#/bin/bash
softhsm2-util --import \
"$HOME/src/github.com/hyperledger/fabric-samples/first-network/crypto-config/peerOrganizations/org1.example.com/users/Admin@org1.example.com/msp/keystore/priv_sk" \
--token "rusty_fabric" --pin "0000" \
--id cafe --label "/org1.example.com/users/Admin@org1.example.com/msp/keystore/priv_sk"
