use std::env;
use std::str::FromStr;
use std::process;
use std::path::{Path, PathBuf};

use rusty_fabric::{Result, create_endorsers};
use rusty_fabric::chaincode;
use rusty_fabric::crypto;
use rusty_fabric::config;
use rusty_fabric::errors;

use rusty_fabric::service::{BroadcastClient, OrdererClient};

fn parse_arguments(args: &[String]) -> Result<(config::Chaincode, String, Option<String>, chaincode::ChaincodeOptions)> {
    // TODO:
    // let policyBytes = createPolicyBytes(signaturePolicy, channelConfigPolicy)?;
    // let ccp = createCollectionConfigPackage(collectionsConfigFile)?;

    let channel_id = args[2].clone();
    if channel_id == "" {
        return Err(errors::new_fabric_error_str("The required parameter 'channelID' is empty. Rerun the command with -C flag"));
    }

    let name = args[3].clone();
    if name == "" {
        return Err(errors::new_fabric_error_str("The required parameter 'name' is empty. Rerun the command with -n flag"));
    }

    let version = args[4].clone();
    if version == "" {
        return Err(errors::new_fabric_error_str("The required parameter 'version' is empty. Rerun the command with -v flag"));
    }

    let init_required = bool::from_str(&args[5]).unwrap();
    let package_id = Some(args[6].clone());
    
    let sequence = i64::from_str(&args[7]).unwrap();
    if sequence == 0 {
        return Err(errors::new_fabric_error_str("The required parameter 'sequence' is empty. Rerun the command with --sequence flag"));
    }

    let chaincode = config::Chaincode{
        name,
        version,
        lang: "".to_string(),
        path: PathBuf::new(),
    };

    let options = chaincode::ChaincodeOptions{
        init_required,
        sequence,
        // validation_parameter: policyBytes,
        // collection_config_package:  ccp,
        ..Default::default()
    };

    return Ok((chaincode, channel_id, package_id, options));
}

fn main() {
	env_logger::init();

    let args: Vec<String> = env::args().collect();
    
    if args.len() < 7 {
        println!("usage: approve_chaincode <config_path> <channel_id> <name> <version> <init_required> <package_id> <sequence>");
        process::exit(1);
    }

    let config_path = Path::new(&args[1]);

    let (chaincode, channel_id, package_id, options) = parse_arguments(&args)
        .expect("Parse input arguments");
    
    let config = rusty_fabric::config::NetworkConfig::read(config_path)
        .expect("Parse config");

    let msp_config = Path::new(&config.identity.msp_config);
    let key_store_path = msp_config.join("keystore");

    // Create CSP
    let csp = crypto::create_oper_csp(&key_store_path);

    // Create Identity
	let identity = crypto::create_identity(&csp, config.identity)
        .expect("Create identity");

    // Create endorsers
    let endorser_clients = create_endorsers(&config.peers.endorsers).
        expect("create endorsers");

    let endorser_clients_refs = endorser_clients.iter().collect();

    let orderer_client = OrdererClient::new(config.orderer)
        .expect("create orderer client");

    let broadcast_client = BroadcastClient::new(orderer_client.get_broadcast_client());

    // TODO:
    let deliver_clients = Vec::new();

    // Client certificate
    // let certificate = open_certificate(certificate_path)
    //  .expect("open certificate");

	chaincode::approve_chaincode(&csp, &identity, &chaincode, &channel_id, package_id, &options, &broadcast_client, endorser_clients_refs, deliver_clients, None)
        .expect("Create channel");
}
