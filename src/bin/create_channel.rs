use std::env;
use std::path::Path;
use std::process;

use rusty_fabric::service::{BroadcastClient, OrdererClient, new_deliver_client_for_orderer};
use rusty_fabric::crypto;
use rusty_fabric::channel;

fn main() {
	env_logger::init();

    let args: Vec<String> = env::args().collect();
    
    if args.len() < 4 {
        println!("usage: create_channel <config_path> <channel_id> <channel_tx>");
        process::exit(1);
    }

    let config_path = Path::new(&args[1]);
    let channel_id = &args[2];
    let channel_tx = Path::new(&args[3]);

    let config = rusty_fabric::config::NetworkConfig::read(config_path)
        .expect("Parse config");

    let msp_config = Path::new(&config.identity.msp_config);
    let key_store_path = msp_config.join("keystore");

    // Create CSP
    let csp = crypto::create_oper_csp(&key_store_path);

    // Create Identity
	let identity = crypto::create_identity(&csp, config.identity)
        .expect("Create identity");

	let orderer_client = OrdererClient::new(config.orderer)
		.expect("Create orderer client");

	let broadcast_client = BroadcastClient::new(orderer_client.get_broadcast_client());

	let deliver_client = new_deliver_client_for_orderer(&csp, &identity, orderer_client.get_broadcast_client(), channel_id, false)
		.expect("Create orderer deliver client");

	channel::create_channel(&csp, &identity, &broadcast_client, &deliver_client, channel_id, channel_tx)
        .expect("Create channel");
}
