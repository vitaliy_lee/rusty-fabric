use std::env;
use std::process;
use std::path::Path;

use fabric_protos_rust::peer;

use rusty_fabric::Result;
use rusty_fabric::crypto;
use rusty_fabric::chaincode;

use rusty_fabric::service::PeerClient;

fn print_response(response: peer::Response, get_installed: bool, channel_id: Option<&str>) -> Result<()> {
    let cqr = protobuf::parse_from_bytes::<peer::ChaincodeQueryResponse>(&response.payload)?;

    if get_installed {
        println!("Get installed chaincodes on peer:");
    } else {
        println!("Get instantiated chaincodes on channel {}:\n", channel_id.unwrap());
    }

    for chaincode_info in &cqr.chaincodes {
        println!("{:?}", chaincode_info);
    }

    return Ok(());
}

fn main() {
	env_logger::init();

    // Read config
    // let opt = Opt::from_args();

    let args: Vec<String> = env::args().collect();
    
    if args.len() < 2 {
        println!("usage: get_chaincodes <config_path>");
        process::exit(1);
    }

    let config_path = Path::new(&args[1]);

    let config = rusty_fabric::config::AppConfig::read(config_path)
        .expect("parse config");

    // let crypto_options = pekan::Options{
    //     library: config.csp.library,
    //     label: config.csp.label,
    //     pin: config.csp.pin,
    //     session_cache_size: Some(3),
    // };

    let peer_client = PeerClient::new(&config.peer)
        .expect("create peer client");

    let endorser_client = peer_client.get_endorser_client();

    // TODO:
    // let sign_certificate_handle = csp.find_certificate(label);
    // let sign_certificate_attrs = csp.get_certificate_attributes(sign_certificate_handle);
    // let ski = sign_certificate_attrs.get(pekan::SKI);

    // let sign_certificate = parse_certificate(sign_cert_data);    
    // let ski = certificate.get_subject_key_id();

    // let ski = b"CAFE";


    // PKCS#12
    // let priv_key_handle = csp.find_key_by_label(config.identity.private_key.as_str(), true)
    //     .expect("find private key");

    // let signer = csp.new_signer(pekan::DigestAlgorithm::SHA256, pekan::SignatureAlgorithm::ECDSA, priv_key_handle)
    //     .expect("get signer");

    // // TODO: pass sign_certificate
    // let identity = Identity::new(signer, config.identity.msp_id, sign_cert_data);

    let msp_config = Path::new(&config.identity.msp_config);
    let key_store_path = msp_config.join("keystore");

    // Create CSP
    let csp = crypto::create_oper_csp(&key_store_path);

    // Create Identity
    let identity = crypto::create_identity(&csp, config.identity)
        .expect("Create identity");

    let response = chaincode::get_chaincodes(&csp, &identity, &endorser_client)
        .expect("Get chaincodes");

    // TODO:
    let channel_id = Some("test-channel");

    print_response(response, false, channel_id)
        .expect("Print response");

    // match result {
    //     Ok(())      => println!("OK"),
    //     Err(error)  => println!("Failed to get chaincodes: {}", error),
    // }
}
