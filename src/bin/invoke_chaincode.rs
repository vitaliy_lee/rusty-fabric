use std::str::FromStr;
use std::env;
use std::path::Path;
use std::process;

use rusty_fabric::service::{BroadcastClient, OrdererClient};
use rusty_fabric::create_endorsers;
use rusty_fabric::chaincode;
use rusty_fabric::crypto;

fn main() {
	env_logger::init();

    let args: Vec<String> = env::args().collect();
    
    if args.len() < 5 {
        println!("usage: get_chaincodes <config_path> <channel_id> <wait_for_event> <request>");
        process::exit(1);
    }

    let config_path = Path::new(&args[1]);

    let config = rusty_fabric::config::NetworkConfig::read(config_path)
        .expect("parse network config");

	// let crypto_options = pekan::Options{
 //        library: config.csp.library.clone(),
 //        label: config.csp.label.clone(),
 //        pin: config.csp.pin.clone(),
 //        session_cache_size: Some(3),
 //    };

    // let csp = Rc::new(Box::new(pekan::CSP::init(crypto_options).unwrap()));

    // PKCS#12
    // let priv_key_handle = csp.find_key_by_label(config.identity.private_key.as_str(), true)
    //     .expect("find private key");

    // let signer = csp.new_signer(fcrypto::DigestAlgorithm::SHA256, fcrypto::SignatureAlgorithm::ECDSA, priv_key_handle)
    //     .expect("get signer");

    // // TODO: pass sign_certificate
    // let identity = Box::new(Identity::new(signer, config.identity.msp_id.clone(), sign_cert_data));

    let msp_config = Path::new(&config.identity.msp_config);
    let key_store_path = msp_config.join("keystore");

    // Create CSP
    let csp = crypto::create_oper_csp(&key_store_path);

    // Create Identity
	let identity = crypto::create_identity(&csp, config.identity)
        .expect("Create identity");

    // TODO: parse arguments
	let channel_id = &args[2];
	let wait_for_event: bool = FromStr::from_str(&args[3]).unwrap();
	// let func = ;
	// let chaincode_args = args[5..].iter().map(|arg| arg.clone()).collect();

	let request: chaincode::ChaincodeInvokeRequest = serde_json::from_str(&args[4])
		.expect("parse JSON request");

	let peer_addresses = Vec::new();

	// Create endorsers
	let endorser_clients = create_endorsers(&config.peers.endorsers).
		expect("create endorsers");

	let endorser_clients_refs = endorser_clients.iter().collect();

    // TODO:
	let deliver_clients = Vec::new();

	let orderer_client = OrdererClient::new(config.orderer)
		.expect("create orderer client");

	let broadcast_client = BroadcastClient::new(orderer_client.get_broadcast_client());

	let chaincode = chaincode::Chaincode{
		name: request.name,
		path: "".to_string(),
		lang: "".to_string(),
		version: "".to_string(),
	};

	let spec = chaincode::get_chaincode_spec(chaincode, &request.func, request.args)
		.expect("get chaincode spec");

	// Client certificate
	// let certificate = open_certificate(certificate_path)
	// 	.expect("open certificate");

	// call with empty txid to ensure production code generates a txid.
	// otherwise, tests can explicitly set their own txid
	let proposal_response = chaincode::invoke_chaincode(
		&csp,
		peer_addresses,
		spec,
		channel_id,
		&identity,
		None,
		endorser_clients_refs,
		deliver_clients,
		&broadcast_client,
		wait_for_event,
	).expect("query chaincode");

	// TODO:
	// if chaincodeQueryRaw {
	// 	fmt.Println(proposal_response.Response.Payload)
	// 	return nil
	// }

	// if chaincodeQueryHex {
	// 	fmt.Printf("%x\n", proposal_response.Response.Payload)
	// 	return nil
	// }

	println!("Response: {:?}", proposal_response.get_response());
}
