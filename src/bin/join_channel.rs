use std::env;
use std::process;
use std::path::Path;

use rusty_fabric::crypto;
use rusty_fabric::channel;
use rusty_fabric::service::PeerClient;

fn main() {
	env_logger::init();

    let args: Vec<String> = env::args().collect();
    
    if args.len() != 3 {
        println!("usage: join_channel <config_path> <genesis_block>");
        process::exit(1);
    }

    let config_path = Path::new(&args[1]);
    let genesis_block = Path::new(&args[2]);

    let config = rusty_fabric::config::AppConfig::read(config_path)
        .expect("Parse config");

    let msp_config = Path::new(&config.identity.msp_config);
    let key_store_path = msp_config.join("keystore");

    // Create CSP
    let csp = crypto::create_oper_csp(&key_store_path);

    // Create Identity
	let identity = crypto::create_identity(&csp, config.identity)
        .expect("Create identity");

    let peer_client = PeerClient::new(&config.peer)
        .expect("create peer client");

    let endorser_client = peer_client.get_endorser_client();

	channel::join_channel(&csp, &identity, genesis_block, &endorser_client)
        .expect("Create channel");
}
