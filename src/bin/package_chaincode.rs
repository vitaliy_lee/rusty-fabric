use std::env;
use std::process;
use std::path::PathBuf;

use rusty_fabric::Result;
use rusty_fabric::chaincode;
use rusty_fabric::config;
use rusty_fabric::errors;
use rusty_fabric::persistence;

fn parse_args(args: &[String]) -> Result<config::ChaincodePackage> {
	if &args[1] == "" {
		return Err(errors::new_fabric_error_str("output file must be specified"));
	}

    let package_path = PathBuf::from(&args[1]);

	if args[2] == "" {
		return Err(errors::new_fabric_error_str("chaincode path must be specified"));
	}

    let source_path = PathBuf::from(&args[2]);

	if args[3] == "" {
		return Err(errors::new_fabric_error_str("package label must be specified"));
	}

    let label = args[3].clone();
	persistence::validate_label(&label)?;

    let lang = "golang".to_string();
	// if lang == "" {
	// 	return Err(errors::new_fabric_error_str("chaincode language must be specified"));
	// }

	let source = config::Chaincode{
		path: source_path,
		lang,
		name: "".to_string(),
		version: "".to_string(),
	};

	let package = config::ChaincodePackage{
		id: "".to_string(),
		path: package_path,
		label,
		source,
	};

	return Ok(package);
}

fn main() {
	env_logger::init();

    let args: Vec<String> = env::args().collect();

    if args.len() < 4 {
        println!("usage: package_chaincode <output_file> <path> <label>");
        process::exit(1);
    }

	let package = parse_args(&args)
						.expect("Parse input arguments");

	chaincode::package_chaincode(&package)
		.expect("Package chaincode");	
}
