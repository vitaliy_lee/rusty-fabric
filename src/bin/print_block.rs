use std::fs;
use std::env;
use std::process;
use std::path::Path;

use protobuf;

use fabric_protos_rust::common;

use rusty_fabric::Result;

fn print_block(block_path: &Path) -> Result<()> {
	let block_data = fs::read(block_path)?;

	let genesis_block = protobuf::parse_from_bytes::<common::Block>(&block_data)?;
	
	let block_header = genesis_block.get_header();

    println!("number: {}", block_header.get_number());

    let previous_hash = block_header.get_previous_hash();
    println!("previous hash: {}", hex::encode(previous_hash));

    let data_hash = block_header.get_data_hash();
    println!("data hash: {}", hex::encode(data_hash));

    return Ok(());
}

fn main() {
	let args: Vec<String> = env::args().collect();
    
    if args.len() < 2 {
        println!("usage: print_block <block_path>");
        process::exit(1);
    }

    let block_path = Path::new(&args[1]);

	print_block(block_path)
		.expect("print block");
}
