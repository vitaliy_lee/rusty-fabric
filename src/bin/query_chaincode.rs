use std::env;
use std::path::Path;
use std::process;

use rusty_fabric::chaincode::{self, Chaincode, ChaincodeInvokeRequest};
use rusty_fabric::crypto;
use rusty_fabric::service::PeerClient;

fn main() {
	env_logger::init();

    let args: Vec<String> = env::args().collect();
    
    if args.len() < 4 {
        println!("usage: get_chaincodes <config_path> <channel_id> <request>");
        process::exit(1);
    }

    let config_path = Path::new(&args[1]);

    let config = rusty_fabric::config::AppConfig::read(config_path)
        .expect("parse config");

    // PKCS#11
	// let crypto_options = pekan::Options{
 //        library: config.csp.library,
 //        label: config.csp.label,
 //        pin: config.csp.pin,
 //        session_cache_size: Some(3),
 //    };

    // let csp = pekan::CSP::init(crypto_options).unwrap();

 //    let priv_key_handle = csp.find_key_by_label(config.identity.private_key.as_str(), true)
 //        .expect("find private key");

	// // TODO: read params from config
 //    let signer = csp.new_signer(fcrypto::DigestAlgorithm::SHA256, fcrypto::SignatureAlgorithm::ECDSA, priv_key_handle)
 //        .expect("get signer");

    // TODO: pass sign_certificate
    // let identity = Box::new(Identity::new(signer, config.identity.msp_id, sign_cert_data));

    let peer_client = PeerClient::new(&config.peer)
        .expect("create peer client");

    let endorser_client = peer_client.get_endorser_client();

    let msp_config = Path::new(&config.identity.msp_config);
    let key_store_path = msp_config.join("keystore");

    // Create CSP
    let csp = crypto::create_oper_csp(&key_store_path);

    // Create Identity
	let identity = crypto::create_identity(&csp, config.identity)
        .expect("Create identity");

	let request: ChaincodeInvokeRequest = serde_json::from_str(&args[3])
		.expect("parse JSON request");

	let chaincode = Chaincode{
		name: request.name,
		path: "".to_string(),
		lang: "".to_string(),
		version: "".to_string(),
	};

	let channel_id = &args[2];

	let spec = chaincode::get_chaincode_spec(chaincode, &request.func, request.args)
		.expect("get chaincode spec");

	// call with empty txid to ensure production code generates a txid.
	// otherwise, tests can explicitly set their own txid
	let proposal_response = chaincode::query_chaincode(&csp, spec, channel_id, &identity, &endorser_client)
		.expect("query chaincode");

	// TODO:
	// if chaincodeQueryRaw {
	// 	fmt.Println(proposal_response.Response.Payload)
	// 	return nil
	// }

	// if chaincodeQueryHex {
	// 	fmt.Printf("%x\n", proposal_response.Response.Payload)
	// 	return nil
	// }

	println!("Response: {:?}", proposal_response.response);
}
