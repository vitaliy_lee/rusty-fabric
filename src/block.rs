use fabric_protos_rust::common;

use crate::Result;
use crate::service;

pub fn get_genesis_block(client: &impl service::DeliverClient) -> Result<common::Block> {
	// timer := time.NewTimer(timeout)
	// defer timer.Stop()

	// 	case <-timer.C:
	// 		cf.DeliverClient.Close()
	// 		return nil, errors.New("timeout waiting for channel creation")


	client.get_specified_block(0)

	// if err != nil {
	// 	cf.DeliverClient.Close()

	// 	time.Sleep(200 * time.Millisecond)
	// 	//try again
	// } else {
	// 	cf.DeliverClient.Close()
	// 	return block, nil
	// }
}
