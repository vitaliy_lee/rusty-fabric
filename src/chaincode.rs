use std::fs;
use std::path::{Path, PathBuf};
use std::io::Write;

use futures::executor;

use tar;

use flate2::write::GzEncoder;
use flate2::Compression;

use protobuf::Message;

use serde::{Serialize, Deserialize};

use fabric_protos_rust::common;
use fabric_protos_rust::peer;
use fabric_protos_rust::peer::lifecycle;

use fcrypto::{CSP, SignerSerializer};

use crate::{Result, APPROVE_FUNC_NAME, COMMIT_FUNC_NAME, sign_proposal, process_proposals};
use crate::errors;
use crate::config;
use crate::packaging;
use crate::proto_util;
use crate::service;
use crate::go;

pub const LIFECYCLE_CHAINCODE_NAME: &str = "_lifecycle";
pub const LSCC_CHAINCODE_NAME: &str = "lscc";
pub const INSTALL_CHAINCODE: &str = "InstallChaincode";
pub const QUERY_INSTALLED_CHAINCODES: &str = "QueryInstalledChaincodes";

const ERRORTHRESHOLD: i32 = 400;

pub struct Chaincode {
    pub name: String,
    pub path: String,
    pub version: String,
    pub lang: String,
}

#[derive(Serialize, Deserialize)]
pub struct ChaincodeInvokeRequest {
    pub name: String,
    pub func: String,
    pub args: Vec<String>,
}

#[derive(Default)]
pub struct ChaincodeOptions<'a> {
    pub sequence:                   i64,
    pub endorsement_plugin:         String,
    pub validation_plugin:          String,
    pub validation_parameter:       Vec<u8>,
    pub collection_config_package:  peer::CollectionConfigPackage,
    pub init_required:              bool,
    pub wait_for_event:             bool,
    pub peer_addresses:             Vec<&'a String>,
    // pub hash:                    Vec<u8>,
    // pub wait_for_event_timeout:  time::Duration,
    // pub tx_id:                   String,
}

pub fn approve_chaincode(
    csp: &impl fcrypto::CSP,
    signer: &impl fcrypto::SignerSerializer,
    chaincode: &config::Chaincode,
    channel_id: &str,
    package_id: Option<String>,
    options: &ChaincodeOptions,
    broadcast_client: &service::BroadcastClient,
    endorser_clients: Vec<&peer::EndorserClient>,
    deliver_clients: Vec<peer::DeliverClient>,
    certificate: Option<tls_api::Certificate>,
) -> Result<()> {
    let channel_id = channel_id.clone();
    let peer_addresses = options.peer_addresses.clone();

    let (tx_id, proposal) = create_approve_proposal(csp, signer, chaincode, channel_id, package_id, options)?;

    let signed_proposal = sign_proposal(&proposal, signer)?;

    let mut responses = Vec::with_capacity(endorser_clients.len());

    for endorser in endorser_clients {
        let grpc_response = endorser.process_proposal(grpc::RequestOptions::new(), signed_proposal.clone()).join_metadata_result();
        let (meta1, proposal_response, meta2) = executor::block_on(grpc_response)?;
        log::debug!("meta1: {:?} meta2: {:?}", meta1, meta2);

        responses.push(proposal_response);
    }

    // all responses will be checked when the signed transaction is created.
    // for now, just set this so we check the first response's status
    let proposal_response = responses[0].clone();

    if proposal_response.response.is_none() {
        return Err(errors::new_fabric_error_str("received proposal response with nil response"));
    }

    let response = proposal_response.response.unwrap();

    if response.status != common::Status::SUCCESS as i32 {
        let message = format!("bad response: {} - {}", response.status, response.message);
        return Err(errors::new_fabric_error(message))
    }

    // assemble a signed transaction (it's an Envelope message)
    let envelope = proto_util::create_signed_tx(&proposal, signer, &responses)?;


    // var dg *chaincode.DeliverGroup
    // var ctx context.Context

    // if input.wait_for_event {
    //     var cancelFunc context.CancelFunc
    //     ctx, cancelFunc = context.WithTimeout(context.Background(), a.Input.WaitForEventTimeout)
    //     defer cancelFunc()

    //     dg = chaincode.NewDeliverGroup(
    //         a.DeliverClients,
    //         a.Input.PeerAddresses,
    //         a.Signer,
    //         a.Certificate,
    //         a.Input.ChannelID,
    //         tx_id,
    //     )

    //     // connect to deliver service on all peers
    //     deliver_group.connect(deliver_clients, certificate)?;
    // }

    if options.wait_for_event {
        // TODO:
        // wait_for_event_timeout
        
        let mut deliver_group = service::DeliverGroup::new(
            csp,
            peer_addresses,
            signer,
            &channel_id,
            &tx_id,
        );

        // connect to deliver service on all peers
        deliver_group.connect(deliver_clients, certificate)?;

        // send the envelope for ordering
        broadcast_client.send(envelope)?;

        // wait for event that contains the txid from all peers
        deliver_group.wait()?;
    } else {
        // send the envelope for ordering
        broadcast_client.send(envelope)?;
    }

    return Ok(());
}

pub fn query_chaincode(csp: &impl fcrypto::CSP,
    spec: peer::ChaincodeSpec,
    channel_id: &str,
    identity: &impl SignerSerializer,
    endorser_client: &peer::EndorserClient,
) -> Result<peer::ProposalResponse> {
    let creator = identity.serialize()?;
    
    // Build the ChaincodeInvocationSpec message
    let mut invocation = peer::ChaincodeInvocationSpec::new();
    invocation.set_chaincode_spec(spec);
    
    // TODO:
    // extract the transient field if it exists
    // var tMap map[string][]byte
    // if transient != "" {
    //  if err := json.Unmarshal([]byte(transient), &tMap); err != nil {
    //      return nil, errors.Wrap(err, "error parsing transient string")
    //  }
    // }

    // let transient_map = HashMap::new();

    let (_, proposal) = proto_util::create_chaincode_proposal_with_tx_id_and_transient(csp, common::HeaderType::ENDORSER_TRANSACTION, channel_id, &invocation, creator, None)?;

    let signed_proposal = sign_proposal(&proposal, identity)?;

    // let responses = process_proposals(endorser_clients, signedProp)?;

    // if responses.len() == 0 {
    //  // this should only happen if some new code has introduced a bug
    //        error!("no proposal responses received - this might indicate a bug");
    //        return Err(Box::new(FabricError))
    // }
    // all responses will be checked when the signed transaction is created.
    // for now, just set this so we check the first response's status
    // return Ok(responses[0]);

    let grpc_resp = endorser_client.process_proposal(grpc::RequestOptions::new(), signed_proposal).join_metadata_result();
    let (meta1, proposal_response, meta2) = executor::block_on(grpc_resp)?;

    debug!("meta1: {:?} meta2: {:?}", meta1, meta2);

    if proposal_response.response.is_none() {
        return Err(errors::new_fabric_error_str("proposal response had nil response"))
    }

    Ok(proposal_response)
}

pub fn package_chaincode(package: &config::ChaincodePackage) -> Result<()> {
    let platforms: Vec<Box<dyn packaging::Platform>> = vec![Box::new(go::Platform::new())];

    let registry = Box::new(packaging::Registry::new(platforms));

    let packager = Packager{
        registry,
    };

    packager.package(package)
}

// Packager holds the dependencies needed to package
// a chaincode and write it
struct Packager {
    registry:   Box<dyn packaging::PlatformRegistry>,
}

// Package packages chaincodes into the package type,
// (.tar.gz) used by _lifecycle and writes it to disk
impl Packager {

    pub fn package(&self, package: &config::ChaincodePackage) -> Result<()> {
        let data = self.get_tar_gz_bytes(package)?;

        fs::write(&package.path, &data).map_err(|err| err.into())
    }

    fn get_tar_gz_bytes(&self, package: &config::ChaincodePackage) -> Result<Vec<u8>> {
        let mut builder = tar::Builder::new(Vec::new());

        let chaincode_type = package.source.lang.to_uppercase();
        let code_path = Path::new(&package.source.path);

        let normalized_path = self.registry.normalize_path(&chaincode_type, code_path)?;

        let metadata = PackageMetadata{
            path: normalized_path,
            cc_type: package.source.lang.clone(),
            label: package.label.clone(),
        };

        let metadata_bytes = serde_json::to_vec(&metadata)?;
        write_data_to_package(&mut builder, "metadata.json".to_string(), &metadata_bytes)?;

        let code_bytes = self.registry.get_deployment_payload(&chaincode_type, code_path)?;
        write_data_to_package(&mut builder, "code.tar.gz".to_string(), &code_bytes)?;

        let data = builder.into_inner()?;

        let mut encoder = GzEncoder::new(Vec::new(), Compression::default());
        encoder.write_all(&data)?;

        encoder.finish().map_err(|err| err.into())
    }

}

fn write_data_to_package(builder: &mut tar::Builder<Vec<u8>>, name: String, payload: &[u8]) -> Result<()> {
    let mut header = tar::Header::new_gnu();
    header.set_path(name)?;
    header.set_mode(0100644);
    header.set_size(payload.len() as u64);
    header.set_cksum();

    builder.append(&header, payload).map_err(|err| err.into())
}

// PackageMetadata holds the path and type for a chaincode package
#[derive(Serialize, Deserialize)]
struct PackageMetadata {
    path:       PathBuf,
    cc_type:    String,
    label:      String,
}

pub fn invoke_chaincode(csp: &impl fcrypto::CSP,
    peer_addresses: Vec<&String>,
    spec: peer::ChaincodeSpec,
    channel_id: &str,
    identity: &impl SignerSerializer,
    certificate: Option<tls_api::Certificate>,
    endorser_clients: Vec<&peer::EndorserClient>,
    deliver_clients: Vec<peer::DeliverClient>,
    broadcast_client: &service::BroadcastClient,
    wait_for_event: bool,
) -> Result<peer::ProposalResponse> {
    let creator = identity.serialize()?;
    
    // Build the ChaincodeInvocationSpec message
    let mut invocation = peer::ChaincodeInvocationSpec::new();
    invocation.set_chaincode_spec(spec);
    
    // TODO:
    // extract the transient field if it exists
    // var tMap map[string][]byte
    // if transient != "" {
    //  if err := json.Unmarshal([]byte(transient), &tMap); err != nil {
    //      return nil, errors.Wrap(err, "error parsing transient string")
    //  }
    // }

    // let transient_map = HashMap::new();

    let (tx_id, proposal) = proto_util::create_chaincode_proposal_with_tx_id_and_transient(csp, common::HeaderType::ENDORSER_TRANSACTION, channel_id, &invocation, creator, None)?;

    let signed_proposal = sign_proposal(&proposal, identity)?;

    let mut responses = process_proposals(endorser_clients, signed_proposal)?;

    if responses.len() == 0 {
        // this should only happen if some new code has introduced a bug
        return Err(errors::new_fabric_error_str("no proposal responses received - this might indicate a bug"))
    }

    // all responses will be checked when the signed transaction is created.
    // for now, just set this so we check the first response's status
    if responses[0].get_response().status >= ERRORTHRESHOLD {
        let message = format!("Response status is unsusccessful: {:?}", responses[0].get_response().get_status());
        return Err(errors::new_fabric_error(message))
    }
            
    // assemble a signed transaction (it's an Envelope message)
    let envelope = proto_util::create_signed_tx(&proposal, identity, &responses)?;

    if wait_for_event {
        // TODO:
        // wait_for_event_timeout
        
        let mut deliver_group = service::DeliverGroup::new(
            csp,
            peer_addresses,
            identity,
            channel_id,
            &tx_id,
        );

        // connect to deliver service on all peers
        deliver_group.connect(deliver_clients, certificate)?;

        // send the envelope for ordering
        broadcast_client.send(envelope)?;

        // wait for event that contains the txid from all peers
        deliver_group.wait()?;
    } else {
        // send the envelope for ordering
        broadcast_client.send(envelope)?;
    }

    let proposal_response = responses.pop().unwrap();

    return Ok(proposal_response);
}

fn create_approve_proposal(
    csp: &impl fcrypto::CSP,
    signer: &impl fcrypto::SignerSerializer,
    chaincode: &config::Chaincode,
    channel_id: &str,
    package_id: Option<String>,
    options: &ChaincodeOptions,
) -> Result<(String, peer::Proposal)> {
    let mut ccsrc = lifecycle::ChaincodeSource::new();

    if package_id.is_none() {
        let unavailable = lifecycle::ChaincodeSource_Unavailable::new();

        ccsrc.set_unavailable(unavailable);
    } else {
        let mut local = lifecycle::ChaincodeSource_Local::new();
        local.set_package_id(package_id.unwrap());

        ccsrc.set_local_package(local);
    }

    let mut approve_args = lifecycle::ApproveChaincodeDefinitionForMyOrgArgs::new();
    approve_args.set_name(chaincode.name.clone());
    approve_args.set_version(chaincode.version.clone());
    approve_args.set_sequence(options.sequence);
    approve_args.set_endorsement_plugin(options.endorsement_plugin.clone());
    approve_args.set_validation_plugin(options.validation_plugin.clone());
    approve_args.set_validation_parameter(options.validation_parameter.clone());
    approve_args.set_init_required(options.init_required);
    approve_args.set_collections(options.collection_config_package.clone());
    approve_args.set_source(ccsrc);

    let approve_args_data = approve_args.write_to_bytes()?;

    let mut chaincode_id = peer::ChaincodeID::new();
    chaincode_id.set_name(LIFECYCLE_CHAINCODE_NAME.to_string());

    let args = vec![APPROVE_FUNC_NAME.as_bytes().into(), approve_args_data];

    let mut chaincode_input = peer::ChaincodeInput::new();
    chaincode_input.set_args(args.into());

    let mut spec = peer::ChaincodeSpec::new();
    spec.set_chaincode_id(chaincode_id);
    spec.set_input(chaincode_input);

    let mut cis = peer::ChaincodeInvocationSpec::new();
    cis.set_chaincode_spec(spec);

    let creator = signer.serialize()?;

    proto_util::create_chaincode_proposal_with_tx_id_and_transient(csp, common::HeaderType::ENDORSER_TRANSACTION, channel_id, &cis, creator, None)
}

pub fn install_chaincode(csp: &impl fcrypto::CSP, signer: &impl fcrypto::SignerSerializer, package: &Path, endorser_client: &peer::EndorserClient) -> Result<()> {
    let package_data = fs::read(package)?;

    let creator = signer.serialize()?;

    let proposal = create_install_proposal(csp, package_data, creator)?;

    let signed_proposal = sign_proposal(&proposal, signer)?;

    submit_install_proposal(endorser_client, signed_proposal)
}

fn create_install_proposal(csp: &impl fcrypto::CSP, package_data: Vec<u8>, creator_data: Vec<u8>) -> Result<peer::Proposal> {
    let mut install_chaincode_args = lifecycle::InstallChaincodeArgs::new();
    install_chaincode_args.set_chaincode_install_package(package_data);

    let install_chaincode_args_data = install_chaincode_args.write_to_bytes()?;

    let args = vec![INSTALL_CHAINCODE.as_bytes().into(), install_chaincode_args_data];

    let mut chaincode_input = peer::ChaincodeInput::new();
    chaincode_input.set_args(args.into());

    let mut chaincode_id = peer::ChaincodeID::new();
    chaincode_id.set_name(LIFECYCLE_CHAINCODE_NAME.to_string());

    let mut chaincode_spec = peer::ChaincodeSpec::new();
    chaincode_spec.set_chaincode_id(chaincode_id);
    chaincode_spec.set_input(chaincode_input);

    let mut cis = peer::ChaincodeInvocationSpec::new();
    cis.set_chaincode_spec(chaincode_spec);

    proto_util::create_proposal_from_cis(csp, common::HeaderType::ENDORSER_TRANSACTION, "", &cis, creator_data)
}

fn submit_install_proposal(endorser_client: &peer::EndorserClient, proposal: peer::SignedProposal) -> Result<()> {
    let grpc_resp = endorser_client.process_proposal(grpc::RequestOptions::new(), proposal).join_metadata_result();
    let (meta1, proposal_response, meta2) = executor::block_on(grpc_resp)?;
    log::debug!("meta1: {:?} meta2: {:?}", meta1, meta2);

    if proposal_response.response.is_none() {
        return Err(errors::new_fabric_error_str("chaincode install failed: received nil proposal response"));
    }

    let response = proposal_response.response.unwrap();

    if response.status != common::Status::SUCCESS as i32 {
        let message = format!("bad response: {} - {}", response.status, response.message);
        return Err(errors::new_fabric_error(message))
    }

    // println!("Installed remotely: {:?}", proposal_response);

    let icr = protobuf::parse_from_bytes::<lifecycle::InstallChaincodeResult>(&response.payload)?;
    println!("Chaincode code package identifier: {}", icr.package_id);

    Ok(())
}
pub fn commit_chaincode(
    csp: &impl fcrypto::CSP,
    signer: &impl fcrypto::SignerSerializer,
    chaincode: &config::Chaincode,
    channel_id: &str,
    options: &ChaincodeOptions,
    broadcast_client: &service::BroadcastClient,
    endorser_clients: Vec<&peer::EndorserClient>,
    deliver_clients: Vec<peer::DeliverClient>,
    certificate: Option<tls_api::Certificate>,
) -> Result<()> {
    let peer_addresses = options.peer_addresses.clone();

    let (tx_id, proposal) = create_commit_proposal(csp, signer, chaincode, channel_id, options)?;

    let signed_proposal = sign_proposal(&proposal, signer)?;

    let mut responses = Vec::with_capacity(endorser_clients.len());

    for endorser in endorser_clients {
        let grpc_response = endorser.process_proposal(grpc::RequestOptions::new(), signed_proposal.clone()).join_metadata_result();
        let (meta1, proposal_response, meta2) = executor::block_on(grpc_response)?;
        log::debug!("meta1: {:?} meta2: {:?}", meta1, meta2);

        responses.push(proposal_response);
    }

    // all responses will be checked when the signed transaction is created.
    // for now, just set this so we check the first response's status
    let proposal_response = responses[0].clone();

    if proposal_response.response.is_none() {
        return Err(errors::new_fabric_error_str("received proposal response with nil response"));
    }

    let response = proposal_response.response.unwrap();

    if response.status != common::Status::SUCCESS as i32 {
        let message = format!("bad response: {} - {}", response.status, response.message);
        return Err(errors::new_fabric_error(message))
    }

    // assemble a signed transaction (it's an Envelope message)
    let envelope = proto_util::create_signed_tx(&proposal, signer, &responses)?;


    // var dg *chaincode.DeliverGroup
    // var ctx context.Context

    // if input.wait_for_event {
    //     var cancelFunc context.CancelFunc
    //     ctx, cancelFunc = context.WithTimeout(context.Background(), a.Input.WaitForEventTimeout)
    //     defer cancelFunc()

    //     dg = chaincode.NewDeliverGroup(
    //         a.DeliverClients,
    //         a.Input.PeerAddresses,
    //         a.Signer,
    //         a.Certificate,
    //         a.Input.ChannelID,
    //         tx_id,
    //     )

    //     // connect to deliver service on all peers
    //     deliver_group.connect(deliver_clients, certificate)?;
    // }

    if options.wait_for_event {
        // TODO:
        // wait_for_event_timeout
        
        let mut deliver_group = service::DeliverGroup::new(
            csp,
            peer_addresses,
            signer,
            channel_id,
            &tx_id,
        );

        // connect to deliver service on all peers
        deliver_group.connect(deliver_clients, certificate)?;

        // send the envelope for ordering
        broadcast_client.send(envelope)?;

        // wait for event that contains the txid from all peers
        deliver_group.wait()?;
    } else {
        // send the envelope for ordering
        broadcast_client.send(envelope)?;
    }

    return Ok(());
}

fn create_commit_proposal(
    csp: &impl fcrypto::CSP,
    signer: &impl fcrypto::SignerSerializer,
    chaincode: &config::Chaincode,
    channel_id: &str,
    options: &ChaincodeOptions,
) -> Result<(String, peer::Proposal)> {
    let mut commit_args = lifecycle::CommitChaincodeDefinitionArgs::new();
    commit_args.set_name(chaincode.name.clone());
    commit_args.set_version(chaincode.version.clone());
    commit_args.set_sequence(options.sequence);
    commit_args.set_endorsement_plugin(options.endorsement_plugin.clone());
    commit_args.set_validation_plugin(options.validation_plugin.clone());
    commit_args.set_validation_parameter(options.validation_parameter.clone());
    commit_args.set_init_required(options.init_required);
    commit_args.set_collections(options.collection_config_package.clone());

    let commit_args_data = commit_args.write_to_bytes()?;

    let mut chaincode_id = peer::ChaincodeID::new();
    chaincode_id.set_name(LIFECYCLE_CHAINCODE_NAME.to_string());

    let args = vec![COMMIT_FUNC_NAME.as_bytes().into(), commit_args_data];

    let mut chaincode_input = peer::ChaincodeInput::new();
    chaincode_input.set_args(args.into());

    let mut spec = peer::ChaincodeSpec::new();
    spec.set_chaincode_id(chaincode_id);
    spec.set_input(chaincode_input);

    let mut cis = peer::ChaincodeInvocationSpec::new();
    cis.set_chaincode_spec(spec);

    let creator = signer.serialize()?;

    proto_util::create_chaincode_proposal_with_tx_id_and_transient(csp, common::HeaderType::ENDORSER_TRANSACTION, channel_id, &cis, creator, None)
}

pub fn get_chaincodes(
    csp: &impl CSP,
    identity: &impl SignerSerializer,
    endorser_client: &peer::EndorserClient,
) -> Result<peer::Response> {
    let creator = identity.serialize()?;

    debug!("Serialized identity: {:?}", creator);

    let proposal = create_query_installed_chaincodes_proposal(csp, creator)?;
    debug!("Proposal: {:?}", proposal);

    let signed_proposal = sign_proposal(&proposal, identity)?;
    debug!("Signed proposal: {:?}", signed_proposal);

    let grpc_resp = endorser_client.process_proposal(grpc::RequestOptions::new(), signed_proposal).join_metadata_result();
    let (meta1, proposal_response, meta2) = executor::block_on(grpc_resp)?;
    debug!("meta1: {:?} meta2: {:?}", meta1, meta2);

    if proposal_response.response.is_none() {
        return Err(errors::new_fabric_error_str("proposal response had nil response"))
    }

    let response = proposal_response.response.unwrap();

    if response.status != common::Status::SUCCESS as i32 {
        let message = format!("bad response: {} - {}", response.status, response.message);
        return Err(errors::new_fabric_error(message))
    }

    Ok(response)
}

pub fn create_query_installed_chaincodes_proposal(csp: &impl fcrypto::CSP, creator: Vec<u8>) -> Result<peer::Proposal> {
    let query_args = peer::lifecycle::QueryInstalledChaincodesArgs::new().write_to_bytes()?;
    let func_name = QUERY_INSTALLED_CHAINCODES.as_bytes().into();

    let args = vec![func_name, query_args];

    let mut input = peer::ChaincodeInput::new();
    input.set_args(args.into());

    let mut chaincode_id = peer::ChaincodeID::new();
    chaincode_id.set_name(LIFECYCLE_CHAINCODE_NAME.to_string());

    let mut chaincode_spec = peer::ChaincodeSpec::new();
    chaincode_spec.set_chaincode_id(chaincode_id);
    chaincode_spec.set_input(input);

    let mut cis = peer::ChaincodeInvocationSpec::new();
    cis.set_chaincode_spec(chaincode_spec);

    proto_util::create_proposal_from_cis(csp, common::HeaderType::ENDORSER_TRANSACTION, "", &cis, creator)
}

/// get_chaincode_spec get chaincode spec from the cli cmd parameters
pub fn get_chaincode_spec(chaincode: Chaincode, func: &str, mut args: Vec<String>) -> Result<peer::ChaincodeSpec> {
    let mut spec = peer::ChaincodeSpec::new();

    args.insert(0, func.to_string());

    let input_args: Vec<Vec<u8>> = args.into_iter().map(|arg| arg.into_bytes()).collect();
    
    let mut input = peer::ChaincodeInput::new();
    input.set_args(input_args.into());

    spec.set_input(input);

    let ftype = peer::ChaincodeSpec_Type::GOLANG;
    spec.set_field_type(ftype);

    let mut chaincode_id = peer::ChaincodeID::new();
    chaincode_id.set_path(chaincode.path);
    chaincode_id.set_name(chaincode.name);
    chaincode_id.set_version(chaincode.version);
    
    spec.set_chaincode_id(chaincode_id);

    return Ok(spec);
}

// create_get_installed_chaincodes_proposal returns a GETINSTALLEDCHAINCODES
// proposal given a serialized identity
pub fn create_get_installed_chaincodes_proposal(csp: &impl fcrypto::CSP, creator: Vec<u8>) -> Result<peer::Proposal> {
    let func_name = "getinstalledchaincodes".as_bytes().into();

    let args = vec![func_name];

    let mut input = peer::ChaincodeInput::new();
    input.set_args(args.into());

    let mut chaincode_id = peer::ChaincodeID::new();
    chaincode_id.set_name(LSCC_CHAINCODE_NAME.to_string());

    let mut chaincode_spec = peer::ChaincodeSpec::new();
    chaincode_spec.set_field_type(peer::ChaincodeSpec_Type::GOLANG);
    chaincode_spec.set_chaincode_id(chaincode_id);
    chaincode_spec.set_input(input);

    let mut lscc_spec = peer::ChaincodeInvocationSpec::new();
    lscc_spec.set_chaincode_spec(chaincode_spec);

    proto_util::create_proposal_from_cis(csp, common::HeaderType::ENDORSER_TRANSACTION, "", &lscc_spec, creator)
}
