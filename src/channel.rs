use std::fs;
use std::path::Path;

use fabric_protos_rust::{common, peer};

use futures::executor;

use protobuf::Message;

use crate::{Result, read_channel_config_update, send_channel_transaction, sign_proposal, JOIN_CHAIN};
use crate::block;
use crate::errors;
use crate::service::{BroadcastClient, DeliverClient};
use crate::proto_util;

pub fn create_channel(
	csp: &impl fcrypto::CSP,
	signer: &impl fcrypto::SignerSerializer,
	broadcast_client: &BroadcastClient,
	deliver_client: &impl DeliverClient,
	channel_id: &str,
	channel_tx: &Path,
) -> Result<()> {
	// TODO:
	// let config_update = create_channel_from_defaults(signer, channel_id)?;
	let config_update = read_channel_config_update(channel_tx)?;

	send_channel_transaction(csp, signer, broadcast_client, config_update, channel_id)?;

	let block = block::get_genesis_block(deliver_client)?;

	let data = block.write_to_bytes()?;
	let file_name = format!("{}.block", channel_id);

	fs::write(file_name, &data).map_err(|err| err.into())
}

pub fn join_channel(csp: &impl fcrypto::CSP, signer: &impl fcrypto::SignerSerializer, genesis_block: &Path, endorser_client: &peer::EndorserClient) -> Result<()> {
    let spec = get_join_cc_spec(genesis_block)?;

    // Build the ChaincodeInvocationSpec message
    let mut invocation = peer::ChaincodeInvocationSpec::new();
    invocation.set_chaincode_spec(spec);

    let creator = signer.serialize()?;
        // return fmt.Errorf("Error serializing identity for %s: %s", cf.Signer.GetIdentifier(), err)

    let proposal = proto_util::create_proposal_from_cis(csp, common::HeaderType::CONFIG, "", &invocation, creator)?;
        // return fmt.Errorf("Error creating proposal for join %s", err)

    let signed_proposal = sign_proposal(&proposal, signer)?;
        // return fmt.Errorf("Error creating signed proposal %s", err)

    let grpc_resp = endorser_client.process_proposal(grpc::RequestOptions::new(), signed_proposal).join_metadata_result();
    let (meta1, proposal_response, meta2) = executor::block_on(grpc_resp)?;
    log::debug!("meta1: {:?} meta2: {:?}", meta1, meta2);

    if proposal_response.response.is_none() {
        return Err(errors::new_fabric_error_str("proposal response had nil response"));
    }

    let response = proposal_response.response.unwrap();

    if response.status != common::Status::SUCCESS as i32 {
        let message = format!("bad response: {} - {}", response.status, response.message);
        return Err(errors::new_fabric_error(message))
    }

    println!("Successfully submitted proposal to join channel");

    Ok(())
}

pub fn update_channel(
    csp: &impl fcrypto::CSP,
    signer: &impl fcrypto::SignerSerializer,
	broadcast_client: &BroadcastClient,
    channel_id: &str,
    channel_tx: &Path,
) -> Result<()> {
	let config_update = read_channel_config_update(channel_tx)?;
	send_channel_transaction(csp, signer, broadcast_client, config_update, channel_id)
}

fn get_join_cc_spec(genesis_block: &Path) -> Result<peer::ChaincodeSpec> {
    let data = fs::read(genesis_block)?;

    let args = vec![JOIN_CHAIN.as_bytes().into(), data];

    // Build the spec
    let mut input = peer::ChaincodeInput::new();
    input.set_args(args.into());

    let mut chaincode_id = peer::ChaincodeID::new();
    chaincode_id.set_name("cscc".to_string());

    let mut spec = peer::ChaincodeSpec::new();
    spec.set_field_type(peer::ChaincodeSpec_Type::GOLANG);
    spec.set_chaincode_id(chaincode_id);
    spec.set_input(input);

    Ok(spec)
}

// fn create_channel_from_defaults(signer: &impl fcrypto::Signer, channel_id: &str) -> Result<common::Envelope> {
// 	let profile = genesisconfig::load(genesisconfig::sample_single_msp_channel_profile)?;
// 	encoder::make_channel_creation_transaction(channel_id, signer, profile)
// }
