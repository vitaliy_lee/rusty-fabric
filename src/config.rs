use std::fs;
use std::path::{Path, PathBuf};
use std::io::prelude::*;

use toml;

use serde::Deserialize;

#[derive(Debug, Clone, Deserialize)]
pub struct AppConfig {
    pub csp: CSP,
    pub identity: Identity,
    pub peer:   Peer,
}

impl AppConfig {

    pub fn read<P: AsRef<Path>>(path: P) -> super::Result<AppConfig> {
        let path = path.as_ref();
        
        let mut config_file = fs::File::open(path)?;

        let mut content = String::new();
        config_file.read_to_string(&mut content)?;

        let app_confg: AppConfig = toml::from_str(&content)?;

        Ok(app_confg)
    }

}

#[derive(Debug, Clone, Deserialize)]
pub struct NetworkConfig {
    pub csp: CSP,
    pub identity: Identity,
    pub peers: Peers,
    pub orderer: Orderer,
}

impl NetworkConfig {

    pub fn read<P: AsRef<Path>>(path: P) -> super::Result<NetworkConfig> {
        let path = path.as_ref();
        
        let mut config_file = fs::File::open(path)?;

        let mut content = String::new();
        config_file.read_to_string(&mut content)?;

        let app_confg: NetworkConfig = toml::from_str(&content)?;

        Ok(app_confg)
    }

}

#[derive(Debug, Clone, Deserialize)]
pub struct Peers {
    pub endorsers: Vec<Peer>,
}

#[derive(Debug, Clone, Deserialize)]
pub struct CSP {
    pub library:    String,
    pub label:      String,
    pub pin:        String,
}

#[derive(Debug, Clone, Deserialize)]
pub struct Identity {
    pub msp_id:         String,
    pub msp_config:     String,
    pub ski:            String,
}

#[derive(Debug, Clone, Deserialize)]
pub struct Peer {
    pub address:            String,
    pub port:               u16,
    pub server_name:        String,
    pub tls_ca_certificate: String,
    // pub client_certificate: PathBuf,
    pub verify_hostname:    bool,
}

#[derive(Debug, Clone, Deserialize)]
pub struct Orderer {
    pub address:            String,
    pub port:               u16,
    pub server_name:        String,
    pub tls_ca_certificate: String,
    pub verify_hostname:    bool,
}

#[derive(Debug, Clone, Deserialize)]
pub struct Channel {
    name:           String,
    tx:             PathBuf,
    genesis_block:  PathBuf,
    // hash_algorithm: String,
}

#[derive(Debug, Clone, Deserialize)]
pub struct Chaincode {
    pub name:       String,
    pub lang:       String,
    pub path:       PathBuf,
    pub version:    String,
}

#[derive(Debug, Clone, Deserialize)]
pub struct ChaincodePackage {
    pub id:         String,
    pub label:      String,
    pub path:       PathBuf,
    pub source:     Chaincode,
}
