use std::path::Path;
use std::fs;
use std::convert::{From, TryFrom};
use std::io::{self, Write};

use ecdsa::{FixedSignature, Asn1Signature};

use num_bigint::BigInt;

use tls_api::{TlsConnector, TlsConnectorBuilder};

use fabric_protos_rust::msp;

use protobuf::Message;

use asn1_der::{DerObject, Asn1Der, FromDerObject};

use pem;

use crate::Result;
use crate::errors;
use crate::config;

const NONCE_LENGTH: u64 = 24;

pub struct Identity {
    signer: Box<dyn fcrypto::Signer>,
    msp_id: String,
    data: Vec<u8>, // Certificate PEM data
}

impl Identity {

    pub fn new(signer: Box<dyn fcrypto::Signer>, msp_id: String, data: Vec<u8>) -> Identity {
        Identity{
            signer,
            msp_id,
            data,
        }
    }
}

impl fcrypto::Signer for &Identity {

    fn sign(&self, message: &[u8]) -> fcrypto::Result<Vec<u8>> {        
        match self.signer.sign(message) {
            Ok(signature)   => convert_signature_to_asn1(signature),
            error           => error
        }
    }

}

impl fcrypto::Serializer for &Identity {
    
    fn serialize(&self) -> fcrypto::Result<Vec<u8>> {
        let mut serialized_identity = msp::SerializedIdentity::new();
        serialized_identity.set_mspid(self.msp_id.clone());
        serialized_identity.set_id_bytes(self.data.clone());
        
        let data = serialized_identity.write_to_bytes()?;
        
        return Ok(data)
    }

}

impl fcrypto::SignerSerializer for &Identity {}

pub struct OpenSSLIdentity {
    msp_id: String,
    signer: Box<dyn fcrypto::Signer>,
    certificate_data: Vec<u8>,
}

impl OpenSSLIdentity {
    
    pub fn new(msp_id: String, signer: Box<dyn fcrypto::Signer>, certificate_data: Vec<u8>) -> OpenSSLIdentity {
        OpenSSLIdentity{
            msp_id,
            signer,
            certificate_data,
        }
    }

}

impl fcrypto::Signer for OpenSSLIdentity {

    fn sign(&self, message: &[u8]) -> fcrypto::Result<Vec<u8>> {
        match self.signer.sign(message) {
            Ok(signature)   => convert_signature_to_low_s(signature),
            error           => error
        }
    }
}

impl fcrypto::Serializer for OpenSSLIdentity {
    
    fn serialize(&self) -> fcrypto::Result<Vec<u8>> {
        let mut serialized_identity = msp::SerializedIdentity::new();
        serialized_identity.set_mspid(self.msp_id.clone());
        serialized_identity.set_id_bytes(self.certificate_data.clone());
        
        let data = serialized_identity.write_to_bytes()?;
        
        return Ok(data)
    }

}

impl fcrypto::SignerSerializer for OpenSSLIdentity {}

// convert_signature_to_asn1 converts signature from compact format to ASN1 DER encoded form.
// compact signatures are 64 bytes; DER signatures are 68-72 bytes
fn convert_signature_to_asn1(mut signature: Vec<u8>) -> Result<Vec<u8>> {
    let length = signature.len();
    let mut sbytes = &mut signature[length/2..];

    let mut s = num_bigint::BigInt::from_bytes_be(num_bigint::Sign::Plus, sbytes);

    s = to_low_s(s);

    let (_, nsbytes) = s.to_bytes_be();

    sbytes.write(&nsbytes).unwrap();

    let fixed_signature: FixedSignature<p256::NistP256> = FixedSignature::try_from(&signature[..])?;
    let asn1_signature = Asn1Signature::from(&fixed_signature);

    Ok(Vec::from(asn1_signature.as_ref()))
}

fn convert_signature_to_low_s(signature: Vec<u8>) -> Result<Vec<u8>> {
    let asn1_signature: Asn1Signature<p256::NistP256> = Asn1Signature::try_from(&signature[..])?;
    let fixed_signature = FixedSignature::from(&asn1_signature);

    convert_signature_to_asn1(Vec::from(fixed_signature.as_ref()))
}

pub fn create_tls_connector(ca_cert_path: &Path, verify_hostname: bool) -> tls_api_openssl::TlsConnector {
    let ca_cert_data = fs::read_to_string(ca_cert_path)
        .expect("read ca cert file");

    let block = pem::parse(ca_cert_data)
        .expect("parse PEM data");

    let ca_cert = tls_api::Certificate::from_der(block.contents);

    let mut builder = tls_api_openssl::TlsConnector::builder()
        .expect("create TLS connector builder");
    builder.add_root_certificate(ca_cert)
        .expect("add_root_certificate");

    builder.set_verify_hostname(verify_hostname)
        .expect("set_verify_hostname");
    builder.build().expect("build TLS connector")
}

fn is_low_s(s: &BigInt) -> bool {
    let p256_params_n = BigInt::parse_bytes(b"115792089210356248762697446949407573529996955224135760342422259061068512044369", 10).unwrap();

    let half_order: BigInt = p256_params_n >> 1;
    
    return s < &half_order;
}

fn to_low_s(s: BigInt) -> BigInt {
    if !is_low_s(&s) {
        let p256_params_n = BigInt::parse_bytes(b"115792089210356248762697446949407573529996955224135760342422259061068512044369", 10).unwrap();

//         // Set s to N - s that will be then in the lower part of signature space
//         // less or equal to half order
        // s.Sub(p256_params_n, s)

        return p256_params_n - s;
    }

    return s;
}

#[derive(Asn1Der, Debug)]
struct PrivateKeyAlgorithmIdentifier {
    algorithm: DerObject,
    parameters: DerObject,
}

#[derive(Asn1Der, Debug)]
struct PKCS8 {
    version: u128,
    private_key_algorithm: PrivateKeyAlgorithmIdentifier,
    private_key: Vec<u8>,
    // attributes: DerObject
}

#[derive(Asn1Der, Debug)]
struct ECPrivateKey {
    version:      u128,
    private_key:  Vec<u8>,
    named_curve_oid: DerObject,
    // public_key:  Vec<u8>,
}

fn open_pkcs8(path: &Path) -> Result<PKCS8> {
    let data = fs::read(path)?;
    let pem = pem::parse(&data)?;

    // PKCS8::deserialize(pem.contents.iter()).map_err(|err| Box::new(err))

    match PKCS8::deserialize(pem.contents.iter()) {
        Ok(pkcs8) => Ok(pkcs8),
        Err(_err) => Err(Box::new(errors::new_fabric_error_str("Failed to deserialize PKCS8")))
    }
}

pub fn read_sign_certificate(signcerts_path: &Path) -> io::Result<Vec<u8>> {
    let signcerts = fs::read_dir(signcerts_path)?;

    let entries = signcerts.map(|res| res.map(|e| e.path()))
        .collect::<io::Result<Vec<_>>>()?;

    let sign_certificate_path = &entries[0];
    fs::read(sign_certificate_path)
}

pub fn get_random_nonce(csp: &impl fcrypto::CSP) -> Result<Vec<u8>> {
    let data = csp.generate_random(NONCE_LENGTH).unwrap();
    return Ok(data);
}

pub fn create_oper_csp(key_store_path: &Path) -> oper::CSP {
    let key_store = oper::FileKeyStore::new(key_store_path);
    oper::CSP::new(key_store)
}

pub fn create_identity(csp: &impl fcrypto::CSP, config: config::Identity) -> Result<OpenSSLIdentity> {
    // TODO: refactor to CertificateStore
    let msp_config = Path::new(&config.msp_config);
    let signcerts_path = msp_config.join("signcerts");

    let sign_cert_data = read_sign_certificate(&signcerts_path)?;
    let ski = hex::decode(&config.ski)?;

    let signer = csp.new_signer(&ski, fcrypto::SignatureAlgorithm::ECDSA)?;

    let identity = OpenSSLIdentity::new(config.msp_id, signer, sign_cert_data);
    Ok(identity)
}

// compute_tx_id computes TxID as the Hash computed
// over the concatenation of nonce and creator.
pub fn compute_tx_id(csp: &impl fcrypto::CSP, nonce: &[u8], creator: &[u8]) -> Result<String> {
    let mut digester = csp.get_digester(fcrypto::DigestAlgorithm::SHA256).unwrap();
    digester.update(nonce)?;
    digester.update(creator)?;
    let digest = digester.digest()?;
    
    let tx_id = hex::encode(digest);

    return Ok(tx_id);
}

#[cfg(test)]
mod tests {

    #[test]
    fn test_is_low_s() {
        use super::is_low_s;

        let low_s = num_bigint::BigInt::parse_bytes(b"5549726246827460293896514036600527129681043004282215751627099378932342201235", 10).unwrap();
        assert_eq!(true, is_low_s(&low_s));

        let high_s = num_bigint::BigInt::parse_bytes(b"86964910569418003317305992085225214893361290747543346255462528275873268476229", 10).unwrap();
        assert_eq!(false, is_low_s(&high_s));
    }

    #[test]
    fn test_to_low_s() {
        use super::to_low_s;

        let high_s = num_bigint::BigInt::parse_bytes(b"86964910569418003317305992085225214893361290747543346255462528275873268476229", 10).unwrap();
        let low_s = num_bigint::BigInt::parse_bytes(b"28827178640938245445391454864182358636635664476592414086959730785195243568140", 10).unwrap();
        
        assert_eq!(low_s, to_low_s(high_s));
    }

    #[test]
    fn test_open_pkcs8() {
        use std::path::Path;

        let test_path = Path::new("testdata/Admin@org1.example.com.key.pem");
        let result = super::open_pkcs8(test_path);
        assert!(result.is_ok());
    }

    #[test]
    fn test_msp_serialized_identity() {
        let mut identity = super::msp::SerializedIdentity::new();
        identity.set_mspid("set_mspid".to_string());

        let data = Vec::new();
        identity.set_id_bytes(data);

        println!("{:?}", identity);
    }

    #[test]
    fn test_ski_calculate() {
        use openssl::hash::{MessageDigest, hash};

        let message = "Fabrik!Fabrik!";

        let message_digest = MessageDigest::sha1();
        let digest = hash(message_digest, message.as_bytes())
            .expect("Hash message");
        
        println!("{:?}", hex::encode(digest.to_vec()));
    }

}

