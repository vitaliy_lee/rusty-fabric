use std::path::{Path, PathBuf};
use std::fs::{self, DirEntry};
use std::io;

use path_calculate::*;

use crate::Result;

// Rel returns a relative path that is lexically equivalent to targpath when joined to basepath with an intervening separator.
// That is, Join(basepath, Rel(basepath, targpath)) is equivalent to targpath itself.
// On success, the returned path will always be relative to basepath, even if basepath and targpath share no elements.
// An error is returned if targpath can't be made relative to basepath or if knowing the current working directory would be necessary to compute it.
// Rel calls Clean on the result.
pub fn relative(base: &Path, target: &Path) -> io::Result<PathBuf> {
	target.related_to(base).map(|path| path.into()).map_err(|err| err.into())
}

// Clean returns the shortest path name equivalent to path by purely lexical processing. It applies the following rules iteratively until no further processing can be done:
//
// 1. Replace multiple Separator elements with a single one.
// 2. Eliminate each . path name element (the current directory).
// 3. Eliminate each inner .. path name element (the parent directory)
//    along with the non-.. element that precedes it.
// 4. Eliminate .. elements that begin a rooted path:
//    that is, replace "/.." by "/" at the beginning of a path,
//    assuming Separator is '/'.
// The returned path ends in a slash only if it represents a root directory, such as "/" on Unix or `C:\` on Windows.
//
// Finally, any occurrences of slash are replaced by Separator.
//
// If the result of this process is an empty string, Clean returns the string ".".
pub fn clean(path: &Path) -> Result<PathBuf> {
	path.canonicalize().map_err(|err| err.into())
}

pub fn walk<F>(dir: &Path, func: &mut F) -> io::Result<()> 
where F: FnMut(&DirEntry) -> io::Result<()> {
    if !dir.is_dir() {
		return Err(io::Error::new(io::ErrorKind::NotFound, "directory doesn't exist"));
	}

    for entry in fs::read_dir(dir)? {
        let entry = entry?;
        let path = entry.path();

		if let Err(err) = func(&entry) {
			if err.kind() == io::ErrorKind::UnexpectedEof {
				continue;
			} else {
				return Err(err);
			}
		}

		if path.is_dir() {
           	walk(&path, func)?;
        } else {
			func(&entry)?;
        }
    }

    return Ok(());
}

#[cfg(test)]
mod tests {
	use std::env;
	use std::path::Path;
	use super::*;

	#[test]
	fn test_relative() {
		// let base = "/a";
		let base = "testdata";

		let paths = hashmap![
			"src" 						=> Some(Path::new("../src").into()),
			"testdata/go_chaincode/src" => Some(Path::new("go_chaincode/src").into()),
			// "/a/b/c"	=> Some(Path::new("b/c").into()),
			// "/b/c"	=> Some(Path::new("../b/c").into()),
			// "./b/c"	=> None,
		];

		for (path, expected) in paths {
			let result = relative(Path::new(base), Path::new(path));
			assert_eq!(expected, result.ok());
		}
	}

	#[test]
	fn test_clean() {
		let current_dir = env::current_dir().unwrap();

		let dirty_path = current_dir.join("./testdata/../Cargo.toml");
		let clean_path = current_dir.join("Cargo.toml");
		
		let result = clean(&dirty_path);
		assert!(result.is_ok());
		assert_eq!(clean_path, result.unwrap());
	}

	#[test]
	fn test_read_dir() {
		use std::fs::{self, DirEntry};

		let entries = fs::read_dir("/Users/tpukep/src/gocrypto")
									.expect("Read dir entries");

		// let files: Vec<DirEntry> = entries
		// 							.map(|entry| entry.unwrap())
		// 							.filter(|entry| entry.file_type().unwrap().is_file())
		// 							.collect();

		// for file in files {
		//     println!("{:?}", file.path());
		// }

		let files: Vec<DirEntry> = entries.map(|entry| entry.unwrap())
				.filter(|entry| entry.file_type().unwrap().is_file())
				.filter(|entry| entry.file_name().to_str().unwrap().ends_with(".go"))
				.inspect(|entry| println!("{:?}", entry.path()))
				.collect();

		assert_eq!(1, files.len());
	}

	#[test]
	fn test_walk() {
		let dir = Path::new("/Users/tpukep/src/gocrypto");
		
		let mut walk_fn = |entry: &DirEntry| {
			let path = entry.path();

			if entry.file_type()?.is_dir() {
				if entry.file_name().to_str().unwrap().starts_with(".") {
					return Err(io::Error::new(io::ErrorKind::UnexpectedEof, "Skip directory"));
				}
			}

			println!("{:?}", path);
			Ok(())
		};

		let result = walk(dir, &mut walk_fn);
		assert!(result.is_ok());
	}

}
