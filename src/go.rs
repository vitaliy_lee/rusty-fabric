use std::collections::{HashMap, HashSet};
use std::fs::{self, DirEntry};
use std::path::{Path, PathBuf};
use std::process;
use std::io::{self, Write};

use serde::{Serialize, Deserialize};

use flate2::write::GzEncoder;
use flate2::Compression;

// use fabric_protos_rust::peer;

use crate::Result;
use crate::errors;
use crate::filepath;
use crate::packaging;

pub struct Platform {

}

impl Platform {

	pub fn new() -> Platform {
		Platform{}
	}
}

impl packaging::Platform for Platform {

	fn get_name(&self) -> String {
		// peer::ChaincodeSpec_Type::GOLANG.to_string();
		"GOLANG".to_string()
	}

	// NormalizePath is used to extract a relative module path from a module root.
	// This should not impact legacy GOPATH chaincode.
	//
	// NOTE: this is only used at the _client_ side by the peer CLI.
	fn normalize_path(&self, path: &Path) -> Result<PathBuf> {
		log::debug!("normalize_path({:?})", path);

		let result = module_info(path)?;

		// not a module
		if result.is_none() {
			return Ok(path.to_path_buf());
		}

		let info = result.unwrap();

		return Ok(info.import_path);
	}

	// get_deployment_payload creates a gzip compressed tape archive that contains the
	// required assets to build and run go chaincode.
	//
	// NOTE: this is only used at the _client_ side by the peer CLI.
	fn get_deployment_payload(&self, codepath: &Path) -> Result<Vec<u8>> {
		log::debug!("get_deployment_payload({:?})", codepath);

		let code_descriptor = describe_code(codepath)?;
		log::debug!("Described: {:?}", code_descriptor);

		let file_map = find_source(&code_descriptor)?;
		log::debug!("Found: {:?}", file_map);

		let mut builder = tar::Builder::new(Vec::new());

		let directories = file_map.get_directories();
		log::debug!("Packing directories: {:?}", directories);

		// Create directories so they get sane ownership and permissions
		for dirname in directories {
			let mut header = tar::Header::new_gnu();

			header.set_entry_type(tar::EntryType::Directory);
			header.set_path(dirname.join(""))?;
			header.set_mode(040000 | 0755);
			header.set_uid(500);
			header.set_gid(500);
			header.set_size(0);
		    header.set_cksum();

			builder.append(&header, &mut io::empty())?;
		}

		let sources = file_map.get_sources();
		log::debug!("Packing files: {:?}", sources);

		for file in sources {
			write_file_to_package(&mut builder, &file.path, &file.name)?;
		}

		let data = builder.into_inner()?;

		let mut encoder = GzEncoder::new(Vec::new(), Compression::default());
		encoder.write_all(&data)?;

		encoder.finish().map_err(|err| err.into())
	}

}

fn write_file_to_package(builder: &mut tar::Builder<Vec<u8>>, path: &Path, name: &Path) -> Result<()> {
	let data = fs::read(path)?;

	// // let mut header = new_file_info_header(path.metadata());
	let mut header = tar::Header::new_gnu();

	// Take the variance out of the tar by using zero time and fixed uid/gid.
	// header.AccessTime = zeroTime
	header.set_mtime(0);
	header.set_mode(0100644);
	header.set_uid(500);
	header.set_gid(500);
	header.set_username("")?;
	header.set_groupname("")?;
	header.set_size(data.len() as u64);
	header.set_cksum();

	builder.append_data(&mut header, name, &data[..]).map_err(|err| err.into())
}

// fn new_file_info_header(metadata: &Metadata) -> tar::Header {
// 	let mut header = tar::Header::new_gnu();
// 	header.set_path(name)?;
// 	header.set_mode(metadata.mode());
// 	header.set_mtime(metadata.modified().unwrap());

// 	//...

// 	return header;
// }

#[derive(Debug, PartialEq)]
struct ModuleInfo {
	dir:			PathBuf,
	go_mod:      	PathBuf,
	import_path: 	PathBuf,
	module_path: 	PathBuf,
}

#[derive(Serialize, Deserialize)]
#[serde(rename_all(deserialize = "PascalCase"))]
struct ModuleData {
	import_path: 	PathBuf,
	module: 		Module,
}

#[derive(Serialize, Deserialize)]
#[serde(rename_all(deserialize = "PascalCase"))]
struct Module {
	dir:	PathBuf,
	path:	PathBuf,
	go_mod: PathBuf,
}

fn module_info(path: &Path) -> Result<Option<ModuleInfo>> {
	if !path.exists() {
		// directory doesn't exist so unlikely to be a module
		return Ok(None)
	}

	// Using `go list -m -f '{{ if .Main }}{{.GoMod}}{{ end }}' all` may try to
	// generate a go.mod when a vendor tool is in use. To avoid that behavior
	// we use `go env GOMOD` followed by an existence check.

	let output = process::Command::new("go")
					            .arg("env")
					            .arg("GOMOD")
					            .current_dir(path)
					            .env("GO111MODULE", "on")
					            .output()?;

	let stdout = String::from_utf8(output.stdout)?;
	let go_mod_path = Path::new(stdout.trim());

	let mod_exists = regular_file_exists(go_mod_path);
	if !mod_exists {
		return Ok(None);
	}

	list_module_info(path).map(|info| Some(info))
}

fn regular_file_exists(path: &Path) -> bool {
	path.is_file()
}

// listModuleInfo extracts module information for the curent working directory.
fn list_module_info(path: &Path) -> Result<ModuleInfo> {
	// log::debug!("list_module_info({:?})", path);

	let output = process::Command::new("go")
					            .arg("list")
					            .arg("-json")
					            .arg(".")
					            .current_dir(path)
					            .env("GO111MODULE", "on")
					            .output()
					            .expect("Execute process");

	if !output.status.success() {
		let message = String::from_utf8(output.stderr).unwrap();
		return Err(errors::new_fabric_error(message));
	}

	// if output.stdout.is_empty() {
	// 	return Err(errors::new_fabric_error_str("command output is empty"));
	// }

	let module_data: ModuleData = serde_json::from_slice(&output.stdout)?;

	let info = ModuleInfo{
		dir:			module_data.module.dir,
		go_mod:      	module_data.module.go_mod,
		import_path: 	module_data.import_path,
		module_path: 	module_data.module.path,
	};

	return Ok(info);
}

// describe_code returns GOPATH and package information.
fn describe_code(path: &Path) -> Result<CodeDescriptor> {
	if path.to_str().unwrap() == "" {
		return Err(errors::new_fabric_error_str("cannot collect files from empty chaincode path"));
	}

	// Use the module root as the source path for go modules
	let result = module_info(path)?;

	if result.is_some() {
		let mod_info = result.unwrap();

		// calculate where the metadata should be relative to module root
		let relative_import = filepath::relative(&mod_info.module_path, &mod_info.import_path)?;

		let desc = CodeDescriptor{
			module:       	true,
			metadata_root: 	mod_info.dir.join(relative_import).join("META-INF"),
			path:         	mod_info.import_path,
			source:       	mod_info.dir,
		};

		return Ok(desc);
	}

	describe_gopath(path)
}

fn describe_gopath(import_path: &Path) -> Result<CodeDescriptor> {
	let output = process::Command::new("go")
					            .arg("list")
					            .arg("-f")
					            .arg("{{.Dir}}")
					            .arg(import_path)
					            .output()?;

	let stdout = String::from_utf8(output.stdout)?;
	let source_path = filepath::clean(Path::new(stdout.trim()))?;

	let desc = CodeDescriptor{
		path:         	import_path.to_path_buf(),
		metadata_root: 	source_path.join("META-INF"),
		source:       	source_path.to_path_buf(),
		module: 		false,
	};

	return Ok(desc);
}

// CodeDescriptor describes the code we're packaging.
#[derive(Default, Debug, PartialEq)]
struct CodeDescriptor {
	source: 		PathBuf, // absolute path of the source to package
	metadata_root: 	PathBuf, // absolute path META-INF
	path: 			PathBuf, // import path of the package
	module:       	bool,   // does this represent a go module
}

impl CodeDescriptor {

	fn is_metadata(&self, path: &Path) -> bool {
		return filepath::clean(path).unwrap()
			.starts_with(filepath::clean(&self.metadata_root).unwrap());
	}

}

#[derive(PartialEq, Eq, PartialOrd, Ord, Debug)]
struct SourceDescriptor {
	name: PathBuf,
	path: PathBuf,
}

#[derive(Debug)]
struct SourceMap {
	data: HashMap<PathBuf, SourceDescriptor>,
}

impl SourceMap {

	fn get_sources(&self) -> Vec<&SourceDescriptor> {
		let mut sources = Vec::new();

		for descriptor in self.data.values() {
			sources.push(descriptor);
		}

		sources.sort();

		return sources
	}

	fn get_directories(&self) -> Vec<&Path> {
		let mut dir_set = HashSet::new();

		for path in self.data.keys() {
			let mut parent = path.parent();

			while let Some(dir) = parent {
				if dir.to_str().unwrap() == "" {
					break;
				}

				dir_set.insert(dir);
				parent = dir.parent();
			}
		}

		let mut dirs: Vec<&Path> = dir_set.drain().collect();

		dirs.sort();

		return dirs;
	}

	fn contains(&self, path: &Path) -> bool {
		self.data.contains_key(path)
	}
}

fn find_source(cd: &CodeDescriptor) -> Result<SourceMap> {
	let mut data = HashMap::new();

	let mut walk_fn = |entry: &DirEntry| -> io::Result<()> {
		let path = entry.path();

		if entry.file_type()?.is_dir() {
			// Allow import of the top level chaincode directory into chaincode code package
			if path == cd.source {
				return Ok(());
			}

			// Allow import of META-INF metadata directories into chaincode code package tar.
			// META-INF directories contain chaincode metadata artifacts such as statedb index definitions
			if cd.is_metadata(&path) {
				return Ok(());
			}

			// include everything except hidden dirs when we're not vendoring
			if cd.module && !entry.file_name().to_str().unwrap().starts_with(".") {
				return Ok(());
			}

			// Do not import any other directories into chaincode code package
			return Err(io::Error::new(io::ErrorKind::UnexpectedEof, "Skip directory"));
		}

		let relative_root =
		if cd.is_metadata(&path) {
			&cd.metadata_root
		} else {
			&cd.source
		};

		let mut name = filepath::relative(&relative_root, &path)?;
			// .map_err(|err| err.into())?;

		if cd.is_metadata(&path) {
			// Skip hidden files in metadata
			if entry.file_name().to_str().unwrap().starts_with(".") {
				return Ok(());
			}

			name = Path::new("META-INF").join(name);
			// TODO:
			// validate_metadata(&name, &path)?;
		} else if cd.module {
			name = Path::new("src").join(name);
		} else {
			// skip top level go.mod and go.sum when not in module mode
			let nm = name.to_str().unwrap();
			if nm == "go.mod" || nm == "go.sum" {
				return Ok(());
			}

			name = Path::new("src").join(&cd.path).join(name);
		}

		// name = filepath.ToSlash(name)

		let descriptor = SourceDescriptor{
			name: name.clone(),
			path: path,
		};

		data.insert(name, descriptor);

		return Ok(());
	};

	filepath::walk(&cd.source, &mut walk_fn)?;

	let sources = SourceMap{
		data,
	};

	return Ok(sources);
}

fn validate_metadata(name: &Path, path: &Path) -> io::Result<()> {
	unimplemented!();
}

// PackageInfo is the subset of data from `go list -deps -json` that's
// necessary to calculate chaincode package dependencies.
// #[derive(Default)]
// struct PackageInfo {
// 	import_path:		String,
// 	dir:				String,
// 	go_files:        	Vec<PathBuf>,
// 	goroot:				bool,
// 	c_files:         	Vec<PathBuf>,
// 	cgo_files:       	Vec<PathBuf>,
// 	h_files:         	Vec<PathBuf>,
// 	s_files:         	Vec<PathBuf>,
// 	ignored_go_files: 	Vec<PathBuf>,
// 	incomplete:     	bool,
// }

// impl PackageInfo {

// 	fn get_files(&self) -> Vec<&Path> {
// 		let mut files = Vec::new();

// 		for file in &self.go_files {
// 			files.push(file.as_path());
// 		}

// 		for file in &self.c_files {
// 			files.push(file.as_path());
// 		}

// 		for file in &self.cgo_files {
// 			files.push(file.as_path());
// 		}

// 		for file in &self.h_files {
// 			files.push(file.as_path());
// 		}

// 		for file in &self.s_files {
// 			files.push(file.as_path());
// 		}

// 		for file in &self.ignored_go_files {
// 			files.push(file.as_path());
// 		}

// 		// append_paths(&mut files, &self.go_files);

// 		return files;
// 	}

// }

// fn append_paths(acc: &mut Vec<&Path>, files: &[PathBuf]) {
// 	for file in files {
// 		acc.push(file.as_path());
// 	}
// }

// fn to_path_buf(file: &str) -> PathBuf {
// 	Path::new(file).to_path_buf()
// }

#[cfg(test)]
mod tests {
	use std::env;
	use std::fs;
	use std::io::Read;
	use std::path::{Path, PathBuf};	

	use tar;

	use flate2::read::GzDecoder;

	use crate::go::{self, find_source, list_module_info, regular_file_exists, describe_code, write_file_to_package, CodeDescriptor, ModuleInfo};
	use crate::packaging::Platform;
	use crate::Result;

	#[test]
	fn test_normalize_path() {
		let temp_dir = env::temp_dir().join("normalize-path");
		fs::create_dir(&temp_dir)
			.expect("Create temporary directory");

		defer! {
			fs::remove_dir(&temp_dir)
				.expect("Remove temporary directory");
		}

		let temp_dir_path = temp_dir.to_str().unwrap();

		let tests = hashmap!{
			"github.com/hyperledger/fabric/cmd/peer" => "github.com/hyperledger/fabric/cmd/peer",
			"testdata/go_chaincode/ccmodule" => "ccmodule",
			"missing" => "missing",
			temp_dir_path => temp_dir_path, // /dev/null is returned from `go env GOMOD`
		};

		let platform = go::Platform::new();

		for (path, expected) in tests {
			let result = platform.normalize_path(Path::new(path));
			assert!(result.is_ok(), "Failed to normalize path {:?}: {:?}", path, result.err().unwrap());

			assert_eq!(Path::new(expected), result.unwrap());
		}
	}

	#[test]
	fn test_list_module_info() {
		let module_dir = Path::new("testdata/go_chaincode/ccmodule").canonicalize().unwrap();

		let mut result = list_module_info(&module_dir);
		assert!(result.is_ok());

		let mut info = result.unwrap();

		let mut expected = ModuleInfo{
			module_path:	Path::new("ccmodule").to_path_buf(),
			import_path:	Path::new("ccmodule").to_path_buf(),
			dir:			module_dir.clone(),
			go_mod:			module_dir.join("go.mod"),
		};

		assert_eq!(expected, info);

		let nested_module_dir = Path::new("testdata/go_chaincode/ccmodule/nested").canonicalize().unwrap();

		result = list_module_info(&nested_module_dir);
		assert!(result.is_ok());

		info = result.unwrap();

		expected = ModuleInfo{
			module_path: Path::new("ccmodule").to_path_buf(),
			import_path: Path::new("ccmodule/nested").to_path_buf(),
			dir:        module_dir.clone(),
			go_mod:		module_dir.join("go.mod"),
		};

		assert_eq!(expected, info);
	}

	#[test]
	fn test_list_module_info_failure() {
		let temp_dir = env::temp_dir().join("module");
		fs::create_dir(&temp_dir)
			.expect("Create temporary directory");

		defer! {
			fs::remove_dir(&temp_dir)
				.expect("Remove temporary directory");
		}
	
		let result = list_module_info(&temp_dir);
		assert!(result.is_err());

		let message = "go: go.mod file not found in current directory or any parent directory; see \'go help modules\'\n";
		assert_eq!(message, result.err().unwrap().to_string());
	}

	// #[test]
	// fn test_package_info_files() {
	// 	let package_info = PackageInfo{
	// 		go_files:        vec!["file1.go", "file2.go"].into_iter().map(to_path_buf).collect(),
	// 		c_files:         vec!["file1.c", "file2.c"].into_iter().map(to_path_buf).collect(),
	// 		cgo_files:       vec!["file_cgo1.go", "file_cgo2.go"].into_iter().map(to_path_buf).collect(),
	// 		h_files:         vec!["file1.h", "file2.h"].into_iter().map(to_path_buf).collect(),
	// 		s_files:         vec!["file1.s", "file2.s"].into_iter().map(to_path_buf).collect(),
	// 		ignored_go_files: vec!["file1_ignored.go", "file2_ignored.go"].into_iter().map(to_path_buf).collect(),
	// 		..Default::default()
	// 	};

	// 	let files = vec![
	// 		"file1.go", "file2.go",
	// 		"file1.c", "file2.c",
	// 		"file_cgo1.go", "file_cgo2.go",
	// 		"file1.h", "file2.h",
	// 		"file1.s", "file2.s",
	// 		"file1_ignored.go", "file2_ignored.go",
	// 	];

	// 	let expected: Vec<&Path> = files.iter().map(|entry| Path::new(entry)).collect();

	// 	assert_eq!(expected, package_info.get_files())
	// }

	#[test]
	fn test_module_deployment_payload() {
		let platform = go::Platform::new();

		// TopLevel
		let mut result = platform.get_deployment_payload(Path::new("testdata/go_chaincode/ccmodule"));
		assert!(result.is_ok());

		let mut deployment_payload = result.unwrap();
		// fs::write("package.tar.gz", &deployment_payload).unwrap();

		let mut contents = tar_contents(deployment_payload).unwrap();
		// println!("{:?}", contents);

		let mut expected: Vec<PathBuf> = vec![
			"META-INF/statedb/couchdb/indexes/indexOwner.json", // top level metadata
			"src/chaincode.go",
			"src/customlogger/customlogger.go",
			"src/go.mod",
			"src/go.sum",
			"src/nested/META-INF/statedb/couchdb/indexes/nestedIndexOwner.json",
			"src/nested/chaincode.go",
		].iter().map(|entry| Path::new(entry).into()).collect();

		assert_eq!(expected, contents);

		// NestedPackage
		result = platform.get_deployment_payload(Path::new("testdata/go_chaincode/ccmodule/nested"));
		assert!(result.is_ok());

		deployment_payload = result.unwrap();
		contents = tar_contents(deployment_payload).unwrap();

		expected = vec![
			"META-INF/statedb/couchdb/indexes/nestedIndexOwner.json", // nested metadata
			"src/META-INF/statedb/couchdb/indexes/indexOwner.json",
			"src/chaincode.go",
			"src/customlogger/customlogger.go",
			"src/go.mod",
			"src/go.sum",
			"src/nested/chaincode.go",
		].iter().map(|entry| Path::new(entry).into()).collect();
		
		assert_eq!(expected, contents);
	}

	#[test]
	fn test_describe_code() {
		use path_calculate::*;

		let abs = Path::new("testdata/go_chaincode/ccmodule").as_absolute_path().unwrap();

		// TopLevelModulePackage
		let mut result = describe_code(Path::new("testdata/go_chaincode/ccmodule"));
		assert!(result.is_ok());

		let mut desc = result.unwrap();

		let mut expected = CodeDescriptor{
			source:			abs.to_path_buf(),
			metadata_root:	abs.join("META-INF"),
			path:			Path::new("ccmodule").to_path_buf(),
			module:			true,
		};

		assert_eq!(expected, desc);

		// NestedModulePackage
		result = describe_code(Path::new("testdata/go_chaincode/ccmodule/nested"));
		assert!(result.is_ok());

		expected = CodeDescriptor{
			source:			abs.to_path_buf(),
			metadata_root:	abs.join("nested").join("META-INF"),
			path:			Path::new("ccmodule/nested").to_path_buf(),
			module:			true,
		};

		desc = result.unwrap();

		assert_eq!(expected, desc);
	}

	#[test]
	fn test_regular_file_exists() {
		// RegularFile
		let mut exists = regular_file_exists(Path::new("testdata/go_chaincode/ccmodule/go.mod"));
		assert!(exists);

		// MissingFile
		exists = regular_file_exists(Path::new("testdata/missing.file"));
		assert!(!exists);

		//Directory
		exists = regular_file_exists(Path::new("testdata"));
		assert!(!exists);
	}

	fn tar_contents(payload: Vec<u8>) -> Result<Vec<PathBuf>> {
		let mut files = Vec::new();

		let mut gz = GzDecoder::new(&payload[..]);
		let mut buffer = Vec::new();
		gz.read_to_end(&mut buffer)?;

		let mut archive = tar::Archive::new(&buffer[..]);

		let entries = archive.entries()?;

		for entry in entries {
	        let file = entry?;

	        let header = file.header();

			if header.entry_type() == tar::EntryType::Regular {
				let path = header.path()?;
				files.push(path.into_owned());
			}
	    }

		return Ok(files);
	}

	#[test]
	fn test_find_source_module() {
		let desc = CodeDescriptor{
			module:       	true,
			source:       	Path::new("testdata/go_chaincode/ccmodule").into(),
			metadata_root: 	Path::new("testdata/go_chaincode/ccmodule/META-INF").into(),
			path:         	Path::new("ccmodule").into(),
		};

		let result = find_source(&desc);
		assert!(result.is_ok());

		let sources = result.unwrap();
		assert_eq!(7, sources.data.len());

		assert!(sources.contains(Path::new("META-INF/statedb/couchdb/indexes/indexOwner.json")));
		assert!(sources.contains(Path::new("src/go.mod")));
		assert!(sources.contains(Path::new("src/go.sum")));
		assert!(sources.contains(Path::new("src/chaincode.go")));
		assert!(sources.contains(Path::new("src/customlogger/customlogger.go")));
		assert!(sources.contains(Path::new("src/nested/chaincode.go")));
		assert!(sources.contains(Path::new("src/nested/META-INF/statedb/couchdb/indexes/nestedIndexOwner.json")));
	}

	#[test]
	fn test_find_source_non_existent() {
		let descriptor = CodeDescriptor{
			path: Path::new("acme.com/this/should/not/exist").into(),
			..Default::default()
		};

		let result = find_source(&descriptor);
		assert!(result.is_err());
		// assert.True(t, os.IsNotExist(errors.Cause(err)))
	}

	#[test]
	fn test_get_directories() {
		let codepath = Path::new("testdata/go_chaincode/ccmodule");

		let code_descriptor = describe_code(codepath).unwrap();

		let file_map = find_source(&code_descriptor).unwrap();
		// println!("Found: {:?}", file_map);

		let directories = file_map.get_directories();
		println!("{:?}", directories);

		assert_eq!(11, directories.len());
	}

	#[test]
	fn test_write_file_to_package() {
		let mut builder = tar::Builder::new(Vec::new());

		let result = write_file_to_package(&mut builder, Path::new("Cargo.toml"), Path::new("Cargo.toml"));
		assert!(result.is_ok());

		let result2 = builder.into_inner();
		assert!(result2.is_ok());

		let data = result2.unwrap();
		assert_eq!(3072, data.len());
	}

}
