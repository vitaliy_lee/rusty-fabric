use std::path::Path;
use std::fs;
use std::error;

use protobuf::well_known_types::Timestamp;
use protobuf::Message;

use chrono::prelude::*;

#[macro_use]
extern crate log;

#[macro_use]
extern crate lazy_static;

#[macro_use]
extern crate maplit;

#[macro_use(defer)]
extern crate scopeguard;

// use ecdsa::{FixedSignature, Asn1Signature};
// use num_bigint::BigInt;
// use tls_api::{TlsConnector, TlsConnectorBuilder};

use futures::executor;

use fabric_protos_rust::{common, peer};

pub mod block;
pub mod chaincode;
pub mod channel;
pub mod config;
pub mod crypto;
pub mod errors;
pub mod filepath;
pub mod go;
pub mod packaging;
pub mod persistence;
pub mod proto_util;
pub mod service;

pub type Result<T> = std::result::Result<T, Box<dyn error::Error>>;

pub const JOIN_CHAIN: &str = "JoinChain";
pub const GET_CONFIG_BLOCK: &str = "GetConfigBlock";
pub const GET_CHANNELS: &str = "GetChannels";

pub const APPROVE_FUNC_NAME: &str = "ApproveChaincodeDefinitionForMyOrg";
pub const COMMIT_FUNC_NAME: &str = "CommitChaincodeDefinition";
pub const CHECK_COMMIT_READINESS_FUNC_NAME: &str = "CheckCommitReadiness";

// fn timestamp_from(time: SystemTime) -> Timestamp {
//     match time.duration_since(SystemTime::UNIX_EPOCH) {
//         Ok(since_epoch) => Timestamp {
//             seconds: since_epoch.as_secs() as i64,
//             nanos: since_epoch.subsec_nanos() as i32,
//             ..Default::default()
//         },
//         Err(e) => {
//             let before_epoch = e.duration();
//             Timestamp {
//                 seconds: -(before_epoch.as_secs() as i64)
//                     - (before_epoch.subsec_nanos() != 0) as i64,
//                 nanos: (1_000_000_000 - before_epoch.subsec_nanos() as i32) % 1_000_000_000,
//                 ..Default::default()
//             }
//         }
//     }
// }

fn timestamp_from(time: DateTime<Utc>) -> Timestamp {
	Timestamp {
			seconds: time.timestamp(),
			nanos: time.timestamp_subsec_nanos() as i32,
			..Default::default()
	}
}

pub fn process_proposals(endorser_clients: Vec<&peer::EndorserClient>, signed_proposal: peer::SignedProposal) -> Result<Vec<peer::ProposalResponse>> {
	let mut proposal_responses = Vec::with_capacity(endorser_clients.len());

	for endorser_client in endorser_clients {
		let grpc_resp = endorser_client.process_proposal(grpc::RequestOptions::new(), signed_proposal.clone()).join_metadata_result();
		let (meta1, proposal_response, meta2) = executor::block_on(grpc_resp)?;
		debug!("meta1: {:?} meta2: {:?}", meta1, meta2);

		proposal_responses.push(proposal_response);
	}

	return Ok(proposal_responses);
}

// sign_proposal returns a signed proposal given a Proposal message and a
// signing identity
pub fn sign_proposal(proposal: &peer::Proposal, signer: &impl fcrypto::Signer) -> Result<peer::SignedProposal> {
    let proposal_bytes =  proposal.write_to_bytes()?;
    debug!("Serialized proposal: {:?}", proposal_bytes);

    let signature = signer.sign(&proposal_bytes)?;
    debug!("Created signature: {:?}", signature);

    let mut signed_proposal = peer::SignedProposal::new();
    signed_proposal.set_proposal_bytes(proposal_bytes);
    signed_proposal.set_signature(signature);

    return Ok(signed_proposal)
}

pub fn open_certificate(path: &Path) -> Result<tls_api::Certificate> {
	let data = fs::read(path)?;

	let block = pem::parse(data).map_err(|err| Box::new(err))?;

	let certificate = tls_api::Certificate::from_der(block.contents);

	return Ok(certificate);
}

pub fn create_endorsers(endorsers: &Vec<config::Peer>) -> Result<Vec<peer::EndorserClient>> {
	let mut endorser_clients = Vec::new();

	for peer in endorsers {
		let peer_client = service::PeerClient::new(peer)?;

		let endorser_client = peer_client.get_endorser_client();
		endorser_clients.push(endorser_client);
	}

	return Ok(endorser_clients);
}

// #[cfg(test)]
// mod tests {
	// use super::*;

  //   #[test]
  //   fn test_peer_proposal() {
  //   	let mut prop = crate::peer::Proposal::new();

  //   	let header_data = Vec::new();
  //   	prop.set_header(header_data);

  //   	let payload_data = Vec::new();
		// prop.set_payload(payload_data);

		// println!("{:?}", prop);
  //   }

// }

pub fn send_channel_transaction(
	csp: &impl fcrypto::CSP,
	signer: &impl fcrypto::SignerSerializer,
	broadcast_client: &service::BroadcastClient,
	mut config_update: common::Envelope,
	channel_id: &str,
) -> Result<()> {
	config_update = check_and_sign_config_tx(csp, signer, config_update, channel_id)?;
	broadcast_client.send(config_update)
}

pub fn read_channel_config_update(path: &Path) -> Result<common::Envelope> {
	let data = fs::read(path)?;
	proto_util::unmarshal_envelope(&data)
}

fn check_and_sign_config_tx(csp: &impl fcrypto::CSP, signer: &impl fcrypto::SignerSerializer, config_update: common::Envelope, channel_id: &str) -> Result<common::Envelope> {
	let payload = proto_util::unmarshal_payload(&config_update.payload)?;

	if payload.header.is_none()  {
		return Err(errors::new_fabric_error_str("bad header"));
	}

	let header = payload.header.unwrap();

	if header.channel_header.is_empty() {
		return Err(errors::new_fabric_error_str("bad channel header"));
	}

	let channel_header = proto_util::unmarshal_channel_header(&header.channel_header)?;

	if channel_header.field_type != common::HeaderType::CONFIG_UPDATE as i32 {
		return Err(errors::new_fabric_error_str("bad type"));
	}

	if channel_header.channel_id == "" {
		return Err(errors::new_fabric_error_str("empty channel id"));
	}

	// Specifying the chainID on the CLI is usually redundant, as a hack, set it
	// here if it has not been set explicitly
	// if channel_id == "" {
	// 	channel_id = &channel_header.channel_id
	// }

	if channel_header.channel_id != channel_id {
		let message = format!("mismatched channel ID {} != {}", channel_header.channel_id, channel_id);
		return Err(errors::new_fabric_error(message));
	}

	let mut config_update = proto_util::unmarshal_config_update_envelope(&payload.data)?;

	let signature_header = proto_util::new_signature_header(csp, signer)?;
	let signature_header_data = signature_header.write_to_bytes()?;

	let data = [&signature_header_data[..], &config_update.config_update[..]].concat();

	let signature = signer.sign(&data)?;

	let mut config_sig = common::ConfigSignature::new();
	config_sig.set_signature_header(signature_header_data);
	config_sig.set_signature(signature);

	let signatures = config_update.mut_signatures();
	signatures.push(config_sig);

	proto_util::create_signed_envelope(csp, common::HeaderType::CONFIG_UPDATE, channel_id.to_string(), Some(signer), &config_update, 0, 0)
}
