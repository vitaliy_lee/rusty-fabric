use std::collections::HashMap;
use std::path::{Path, PathBuf};

use crate::Result;
use crate::errors;

// PlatformRegistry defines the interface to get the code bytes
// for a chaincode given the type and path
pub trait PlatformRegistry {

	fn normalize_path(&self, cc_type: &str, path: &Path) -> Result<PathBuf>;

	fn get_deployment_payload(&self, cc_type: &str, path: &Path) -> Result<Vec<u8>>;

}

// Interface for validating the specification and writing the package for
// the given platform
pub trait Platform {

	fn get_name(&self) -> String;

	// fn validate_path(&self, path: &Path) -> Result<()>;
	
	// fn validate_code_package(&self, code: Vec<u8>) -> Result<()>;

	fn get_deployment_payload(&self, path: &Path) -> Result<Vec<u8>>;

	fn normalize_path(&self, path: &Path) -> Result<PathBuf>;

}

pub struct Registry {
	platforms: HashMap<String, Box<dyn Platform>>,
}

impl Registry {

	pub fn new(platform_types: Vec<Box<dyn Platform>>) -> Registry {
		let mut platforms = HashMap::with_capacity(platform_types.len());

		for platform in platform_types {
			let name = platform.get_name();

			if platforms.contains_key(&name) {
				panic!("Multiple platforms of the same name specified: {}", name);
			}

			platforms.insert(name, platform);
		}

		Registry{
			platforms,
		}
	}

	fn get_platform(&self, cc_type: &str) -> Result<&Box<dyn Platform>> {
		let result = self.platforms.get(cc_type);
		if result.is_none() {
			let message = format!("unknown chaincode type: {}", cc_type);
			return Err(errors::new_fabric_error(message));
		}

		return Ok(result.unwrap());
	}
}

impl PlatformRegistry for Registry {

	fn normalize_path(&self, cc_type: &str, path: &Path) -> Result<PathBuf> {
		let platform = self.get_platform(cc_type)?;
		platform.normalize_path(path)
	}

	fn get_deployment_payload(&self, cc_type: &str, path: &Path) -> Result<Vec<u8>> {
		let platform = self.get_platform(cc_type)?;
		platform.get_deployment_payload(path)
	}

}
