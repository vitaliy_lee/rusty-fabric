use regex::Regex;

use crate::Result;
use crate::errors;

// LABEL_PATTERN is the regular expression controlling the allowed characters
// for the package label.
const LABEL_PATTERN: &str = r"^[[:alnum:]][[:alnum:]_.+-]*$";

lazy_static! {
    static ref RE: Regex = Regex::new(LABEL_PATTERN).unwrap();
}

// validate_label return an error if the provided label contains any invalid
// characters, as determined by LabelRegexp.
pub fn validate_label(label: &str) -> Result<()> {
	if RE.is_match(label) {
		Ok(())
	} else {
		let message = format!("invalid label '{}'. Label must be non-empty, can only consist of alphanumerics, symbols from '.+-_', and can only begin with alphanumerics", label);
		Err(errors::new_fabric_error(message))
	}
}
