use std::collections::HashMap;

use protobuf::ProtobufError;
use protobuf::Message;

use chrono::prelude::*;

use fabric_protos_rust::{common, orderer, peer};

use crate::Result;
use crate::crypto;
use crate::errors;
use crate::timestamp_from;

pub fn unmarshal_envelope(data: &[u8]) -> Result<common::Envelope> {
    protobuf::parse_from_bytes::<common::Envelope>(data).map_err(wrap_error)
}

pub fn unmarshal_payload(data: &[u8]) -> Result<common::Payload> {
    protobuf::parse_from_bytes::<common::Payload>(data).map_err(wrap_error)
}

pub fn unmarshal_channel_header(data: &[u8]) -> Result<common::ChannelHeader> {
    protobuf::parse_from_bytes::<common::ChannelHeader>(data).map_err(wrap_error)
}

pub fn new_signature_header(csp: &impl fcrypto::CSP, signer: &impl fcrypto::SignerSerializer) -> Result<common::SignatureHeader> {
    let creator = signer.serialize()?;

    let nonce = crypto::get_random_nonce(csp)?;

    let mut header = common::SignatureHeader::new();
    header.set_creator(creator);
    header.set_nonce(nonce);

    return Ok(header);    
}

pub fn unmarshal_config_update_envelope(data: &[u8]) -> Result<common::ConfigUpdateEnvelope> {
    protobuf::parse_from_bytes::<common::ConfigUpdateEnvelope>(data).map_err(wrap_error)
}

// Creates a signed envelope of the desired type, with
// marshaled dataMsg and signs it
pub fn create_signed_envelope(
    csp: &impl fcrypto::CSP,
    tx_type: common::HeaderType,
    channel_id: String,
    signer: Option<&impl fcrypto::SignerSerializer>,
    data_msg: &dyn protobuf::Message,
    msg_version: i32,
    epoch: u64,
) -> Result<common::Envelope> {
    create_signed_envelope_with_tls_binding(csp, tx_type, channel_id, signer, data_msg, msg_version, epoch, None)
}

// Creates a signed envelope of the desired
// type, with marshaled dataMsg and signs it. It also includes a TLS cert hash
// into the channel header
pub fn create_signed_envelope_with_tls_binding(
    csp: &impl fcrypto::CSP,
    tx_type: common::HeaderType,
    channel_id: String,
    signer: Option<&impl fcrypto::SignerSerializer>,
    data_msg: &dyn protobuf::Message,
    msg_version: i32,
    epoch: u64,
    tls_cert_hash: Option<Vec<u8>>,
) -> Result<common::Envelope> {
    let mut payload_channel_header = make_channel_header(tx_type, msg_version, channel_id, epoch);
    
    if tls_cert_hash.is_some() {
        payload_channel_header.set_tls_cert_hash(tls_cert_hash.unwrap());
    }

    let payload_signature_header = 
        if signer.is_some() {
            new_signature_header(csp, signer.unwrap())?
        } else {
            common::SignatureHeader::new()
        };

    let data = data_msg.write_to_bytes()?;
    let payload_header = make_payload_header(payload_channel_header, payload_signature_header)?;
    
    let mut payload = common::Payload::new();
    payload.set_header(payload_header);
    payload.set_data(data);

    let payload_bytes = payload.write_to_bytes()?;

    let mut envelope = common::Envelope::new();

    if signer.is_some() {
        let signature = signer.unwrap().sign(&payload_bytes)?;
        envelope.set_signature(signature);
    }

    envelope.set_payload(payload_bytes);

    return Ok(envelope);
}

// create_proposal_from_cis returns a proposal given a serialized identity and a
// ChaincodeInvocationSpec
pub fn create_proposal_from_cis(csp: &impl fcrypto::CSP, typ: common::HeaderType, channel_id: &str, cis: &peer::ChaincodeInvocationSpec, creator: Vec<u8>) -> Result<peer::Proposal> {
    create_chaincode_proposal(csp, typ, channel_id, cis, creator)
}

// create_chaincode_proposal creates a proposal from given input.
// It returns the proposal and the transaction id associated to the proposal
pub fn create_chaincode_proposal(csp: &impl fcrypto::CSP, typ: common::HeaderType, channel_id: &str, cis: &peer::ChaincodeInvocationSpec, creator: Vec<u8>) -> Result<peer::Proposal> {
    create_chaincode_proposal_with_transient(csp, typ, channel_id, cis, creator, None)
}

// create_chaincode_proposal_with_transient creates a proposal from given input
// It returns the proposal and the transaction id associated to the proposal
pub fn create_chaincode_proposal_with_transient(csp: &impl fcrypto::CSP, typ: common::HeaderType, channel_id: &str, cis: &peer::ChaincodeInvocationSpec, creator: Vec<u8>, transient_map: Option<HashMap<String, Vec<u8>>>) -> Result<peer::Proposal> {
    // generate a random nonce
    let nonce = crypto::get_random_nonce(csp)?;

    // compute txid
    let tx_id = crypto::compute_tx_id(csp, &nonce, &creator)?;

    return create_chaincode_proposal_with_tx_id_nonce_and_transient(&tx_id, typ, channel_id, cis, nonce, creator, transient_map)
}

// create_chaincode_proposal_with_tx_id_and_transient creates a proposal from given
// input. It returns the proposal and the transaction id associated with the
// proposal
pub fn create_chaincode_proposal_with_tx_id_and_transient(csp: &impl fcrypto::CSP, typ: common::HeaderType, channel_id: &str, cis: &peer::ChaincodeInvocationSpec, creator: Vec<u8>, transient_map: Option<HashMap<String, Vec<u8>>>) -> Result<(String, peer::Proposal)> {
    // generate a random nonce
    let nonce = crypto::get_random_nonce(csp)?;

    // compute txid
    let tx_id = crypto::compute_tx_id(csp, &nonce, &creator)?;

    let proposal = create_chaincode_proposal_with_tx_id_nonce_and_transient(&tx_id, typ, channel_id, cis, nonce, creator, transient_map)?;

    Ok((tx_id, proposal))
}

// create_chaincode_proposal_with_tx_id_nonce_and_transient creates a proposal from
// given input
pub fn create_chaincode_proposal_with_tx_id_nonce_and_transient(tx_id: &str, typ: common::HeaderType, channel_id: &str, cis: &peer::ChaincodeInvocationSpec, nonce: Vec<u8>, creator: Vec<u8>, transient_map: Option<HashMap<String, Vec<u8>>>) -> Result<peer::Proposal> {
    let mut chaincode_header_ext = peer::ChaincodeHeaderExtension::new();
    chaincode_header_ext.set_chaincode_id(cis.get_chaincode_spec().get_chaincode_id().clone());

    let mut chaincode_proposal_payload = peer::ChaincodeProposalPayload::new();

    let cis_data = cis.write_to_bytes().unwrap();
    chaincode_proposal_payload.set_input(cis_data);

    if transient_map.is_some() {
        chaincode_proposal_payload.set_TransientMap(transient_map.unwrap());
    }

    let payload_data = chaincode_proposal_payload.write_to_bytes().unwrap();

    let mut chan_header = common::ChannelHeader::new();
    chan_header.set_field_type(typ as i32);
    chan_header.set_tx_id(tx_id.to_string());
    
    let timestamp = timestamp_from(Utc::now());

    chan_header.set_timestamp(timestamp);
    chan_header.set_channel_id(channel_id.to_string());
    
    let chaincode_header_ext_data = chaincode_header_ext.write_to_bytes().unwrap();
    chan_header.set_extension(chaincode_header_ext_data);

    // TODO: epoch is now set to zero. This must be changed once we
    // get a more appropriate mechanism to handle it in.
    let epoch = 0;
    chan_header.set_epoch(epoch);

    let mut sign_header = common::SignatureHeader::new();
    sign_header.set_nonce(nonce);
    sign_header.set_creator(creator);

    let mut header = common::Header::new();

    let chan_header_data = chan_header.write_to_bytes().unwrap();
    header.set_channel_header(chan_header_data);

    let sign_header_data = sign_header.write_to_bytes().unwrap();    
    header.set_signature_header(sign_header_data);

    let mut prop = peer::Proposal::new();

    let header_data = header.write_to_bytes().unwrap();
    prop.set_header(header_data);

    prop.set_payload(payload_data);

    return Ok(prop)
}


pub fn create_signed_tx(proposal: &peer::Proposal, signer: &impl fcrypto::Signer, responses: &[peer::ProposalResponse]) -> Result<common::Envelope> {
    println!("Processing {:?} responses", responses.len());
    
    if responses.len() == 0 {
        return Err(errors::new_fabric_error_str("at least one proposal response is required"))
    }

    // the original header
    let header = protobuf::parse_from_bytes::<common::Header>(&proposal.header)?;

    // the original payload
    let proposal_payload = protobuf::parse_from_bytes::<peer::ChaincodeProposalPayload>(&proposal.payload)?;

    // ensure that all actions are bitwise equal and that they are successful
    let response_payload = &responses[0].get_response().payload[..];

    for proposal_response in responses.iter() {
        let response = proposal_response.get_response();

        if response.status < 200 || response.status >= 400 {
            let message = format!("proposal response was not successful, error code {}, msg {}", response.status, response.message);
            return Err(errors::new_fabric_error(message));
        }

        if response_payload != &response.payload[..] {
            return Err(errors::new_fabric_error_str("ProposalResponse payloads do not match"));
        }
    }

    // fill endorsements
    let endorsements: Vec<peer::Endorsement> = responses.into_iter().map(|resp| resp.get_endorsement().clone()).collect();

    println!("Packing {:?} endorsements", endorsements.len());

    // create ChaincodeEndorsedAction
    let mut cea = peer::ChaincodeEndorsedAction::new();
    cea.set_proposal_response_payload(responses[0].payload.clone());
    cea.set_endorsements(endorsements.into());

    // obtain the bytes of the proposal payload that will go to the transaction
    let proposal_payload_bytes = proposal_payload.write_to_bytes()?;

    // serialize the chaincode action payload
    let mut cap = peer::ChaincodeActionPayload::new();
    cap.set_chaincode_proposal_payload(proposal_payload_bytes);
    cap.set_action(cea);

    let cap_bytes = cap.write_to_bytes()?;

    // create a transaction
    let mut taa = peer::TransactionAction::new();
    taa.set_header(header.signature_header.clone());
    taa.set_payload(cap_bytes);
    
    let taas = vec![taa];
    
    let mut transaction = peer::Transaction::new();
    transaction.set_actions(taas.into());

    // serialize the transaction
    let tx_bytes = transaction.write_to_bytes()?;

    // create the payload
    let mut payload = common::Payload::new();
    payload.set_header(header);
    payload.set_data(tx_bytes);

    let payload_bytes = payload.write_to_bytes()?;

    // sign the payload
    let signature = signer.sign(&payload_bytes)?;

    // here's the envelope
    let mut envelope = common::Envelope::new();
    envelope.set_payload(payload_bytes);
    envelope.set_signature(signature);

    return Ok(envelope);
}

fn wrap_error(error: ProtobufError) -> Box<dyn std::error::Error> {
    Box::new(error)
}

fn make_channel_header(header_type: common::HeaderType, version: i32, channel_id: String, epoch: u64) -> common::ChannelHeader {
    let mut header = common::ChannelHeader::new();
    header.set_field_type(header_type as i32);
    header.set_version(version);
    
    let timestamp = timestamp_from(Utc::now());
    header.set_timestamp(timestamp);

    header.set_channel_id(channel_id);
    header.set_epoch(epoch);

    return header;
}

fn make_payload_header(channel_header: common::ChannelHeader, signature_header: common::SignatureHeader) -> Result<common::Header> {
    let channel_header_bytes = channel_header.write_to_bytes()?;
    let signature_header_bytes = signature_header.write_to_bytes()?;

    let mut header = common::Header::new();
    header.set_channel_header(channel_header_bytes);
    header.set_signature_header(signature_header_bytes);

    return Ok(header);
}

pub fn create_deliver_envelope(
    csp: &impl fcrypto::CSP,
    signer: &impl fcrypto::SignerSerializer,
    channel_id: String,
    certificate: Option<tls_api::Certificate>,
) -> Result<common::Envelope> {
    // check for client certificate and create hash if present
    let tls_certificate_hash = certificate.map(|cert| {
        let cert_data = cert.into_der().unwrap();
        csp.digest(&cert_data, fcrypto::DigestAlgorithm::SHA256).unwrap()
    });

    let mut start = orderer::ab::SeekPosition::new();
    start.set_newest(orderer::ab::SeekNewest::new());

    let mut seek_specified = orderer::ab::SeekSpecified::new();
    seek_specified.set_number(u64::MAX);

    let mut stop = orderer::ab::SeekPosition::new();
    stop.set_specified(seek_specified);

    let mut seek_info = orderer::ab::SeekInfo::new();
    seek_info.set_start(start);
    seek_info.set_stop(stop);
    seek_info.set_behavior(orderer::ab::SeekInfo_SeekBehavior::BLOCK_UNTIL_READY);

    create_signed_envelope_with_tls_binding(
        csp,
        common::HeaderType::DELIVER_SEEK_INFO,
        channel_id,
        Some(signer),
        &seek_info,
        0 as i32,
        0 as u64,
        tls_certificate_hash,
    )
}
