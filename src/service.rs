use std::sync::Arc;
use std::thread;
use std::path::Path;

use crate::Result;
use crate::{config, crypto, errors, proto_util};

use grpc::ClientStub;

use futures::executor;
use futures_util::stream::StreamExt;

use fabric_protos_rust::orderer::{self, ab};
use fabric_protos_rust::common;
use fabric_protos_rust::peer;

pub struct PeerClient {
	grpc_client: Arc<grpc::Client>,
}

impl PeerClient {

	pub fn new(config: &config::Peer) -> Result<PeerClient> {
		let tls_connector = Arc::new(crypto::create_tls_connector(Path::new(&config.tls_ca_certificate), config.verify_hostname));
		let tls_option = httpbis::ClientTlsOption::Tls(config.server_name.clone(), tls_connector);
		
		let grpc_client = Arc::new(
			grpc::ClientBuilder::new(&config.address, config.port)
				.explicit_tls(tls_option)
				.build()?,
		);

		let peer_client = PeerClient{
			grpc_client,
		};

		return Ok(peer_client);
	}

	pub fn get_endorser_client(&self) -> peer::EndorserClient {
		peer::EndorserClient::with_client(self.grpc_client.clone())
	}

	pub fn get_deliver_client(&self) -> peer::DeliverClient {
		peer::DeliverClient::with_client(self.grpc_client.clone())
	}
}

pub struct OrdererClient {
	grpc_client: Arc<grpc::Client>,
}

impl OrdererClient {
	
	pub fn new(config: config::Orderer) -> Result<OrdererClient> {
		let tls_connector = Arc::new(crypto::create_tls_connector(Path::new(&config.tls_ca_certificate), config.verify_hostname));
		let tls_option = httpbis::ClientTlsOption::Tls(config.server_name.clone(), tls_connector);
		
		let grpc_client = Arc::new(
				grpc::ClientBuilder::new(&config.address, config.port)
					.explicit_tls(tls_option)
					.build()?,
		);

		let orderer_client = OrdererClient{
			grpc_client,
		};

		return Ok(orderer_client);
	}

	pub fn get_broadcast_client(&self) -> orderer::AtomicBroadcastClient {
		orderer::AtomicBroadcastClient::with_client(self.grpc_client.clone())
	}

}

pub trait DeliverClient {
	fn get_specified_block(&self, num: u64) -> Result<common::Block>;

	fn get_oldest_block(&self) -> Result<common::Block>;
	
	fn get_newest_block(&self) -> Result<common::Block>;

	fn close(&self) -> Result<()>;
}

pub fn new_deliver_client_for_peer<'a, C: fcrypto::CSP, S: fcrypto::SignerSerializer>(csp: &'a C, signer: &'a S, channel_id: &'a str, config: &config::Peer, best_effort: bool) -> Result<PeerDeliverClient<'a, C, S>> {
	let peer_client = PeerClient::new(config)?;

	// pc, err := newPeerClientForClientConfig(address, address_override, config);
	// if err != nil {
	// 	return nil, errors.WithMessage(err, "failed to create deliver client for peer")
	// }

	let service = peer_client.get_deliver_client();
	// deliver_client.deliver();

	// TODO: check for client certificate and create hash if present

	let tls_cert_hash = None;
	// if config.cleint_certificate != "" {
	// 	Some(util::compute_hash(deliver_client.Certificate().CertificateChain[0]))
	// } else {
	// 	None
	// };

	// p := &DeliverClient{
	// 	Signer:      signer,
	// 	Service:     ds,
	// 	ChannelID:   channelID,
	// 	TLSCertHash: tlsCertHash,
	// 	BestEffort:  bestEffort,
	// }

	let deliver_client = PeerDeliverClient::new(csp, signer, channel_id, service, tls_cert_hash, best_effort);

	return Ok(deliver_client);
}

pub fn new_deliver_client_for_orderer<'a, C: fcrypto::CSP, S: fcrypto::SignerSerializer>(csp: &'a C, signer: &'a S, broadcast_client: orderer::AtomicBroadcastClient, channel_id: &'a str, best_effort: bool) -> Result<OrdererDeliverClient<'a, C, S>> {
	let tls_cert_hash = None;

	let deliver_client = OrdererDeliverClient::new(csp, signer, channel_id, broadcast_client, tls_cert_hash, best_effort);

	return Ok(deliver_client);
}

// fn newPeerClientForClientConfig(address, address_override string, clientConfig comm.ClientConfig) (*PeerClient, error) {
	// set the default keepalive options to match the server
	// clientConfig.KaOpts = comm.DefaultKeepaliveOptions

	// gClient, err := comm.NewGRPCClient(clientConfig)
	// if err != nil {
	// 	return nil, errors.WithMessage(err, "failed to create PeerClient from config")
	// }

	// pClient := &PeerClient{
	// 	CommonClient: CommonClient{
	// 		GRPCClient: gClient,
	// 		Address:    address,
	// 		sn:         address_override,
	// 	},
	// }

// 	return pClient, nil
// }


// type DeliverClient struct {
// 	Signer      identity.SignerSerializer
// 	Service     ab.AtomicBroadcast_DeliverClient
// 	ChannelID   string
// 	TLSCertHash []byte
// 	BestEffort  bool
// }

pub struct PeerDeliverClient<'a, C: fcrypto::CSP, S: fcrypto::SignerSerializer> {
	csp: &'a C,
	signer: &'a S,
	channel_id:	&'a str,
	deliver_client: peer::DeliverClient,
	tls_cert_hash: Option<Vec<u8>>,
	best_effort:  bool,
}

impl<C: fcrypto::CSP, S: fcrypto::SignerSerializer> PeerDeliverClient<'_, C, S> {

	pub fn new<'a>(csp: &'a C, signer: &'a S, channel_id: &'a str, deliver_client: peer::DeliverClient, tls_cert_hash: Option<Vec<u8>>, best_effort: bool) -> PeerDeliverClient<'a, C, S> {
		PeerDeliverClient{
			csp,
			signer,
			channel_id,
			deliver_client,
			tls_cert_hash,
			best_effort,
		}
	}

}

impl<C: fcrypto::CSP, S: fcrypto::SignerSerializer> DeliverClient for PeerDeliverClient<'_, C, S> {

	// GetSpecifiedBlock gets the specified block from a peer/orderer's deliver service
	fn get_specified_block(&self, num: u64) -> Result<common::Block> {
		let envelope = seek_specified(self.csp, self.signer, self.channel_id, num, self.tls_cert_hash.clone(), self.best_effort)?;
		
		let deliver_response = executor::block_on(peer_deliver(&self.deliver_client, envelope))?;
		read_peer_block(deliver_response)
	}

	fn get_oldest_block(&self) -> Result<common::Block> {
		unimplemented!();
	}
	
	fn get_newest_block(&self) -> Result<common::Block> {
		unimplemented!();
	}

	fn close(&self) -> Result<()> {
		unimplemented!();
	}

}

pub struct OrdererDeliverClient<'a, C: fcrypto::CSP, S: fcrypto::SignerSerializer> {
	csp: &'a C,
	signer: &'a S,
	channel_id:	&'a str,
	broadcast_client: orderer::AtomicBroadcastClient,
	tls_cert_hash: Option<Vec<u8>>,
	best_effort:  bool,
}

impl<C: fcrypto::CSP, S: fcrypto::SignerSerializer> OrdererDeliverClient<'_, C, S> {

	pub fn new<'a>(csp: &'a C, signer: &'a S, channel_id: &'a str, broadcast_client: orderer::AtomicBroadcastClient, tls_cert_hash: Option<Vec<u8>>, best_effort: bool) -> OrdererDeliverClient<'a, C, S> {
		OrdererDeliverClient{
			csp,
			signer,
			channel_id,
			broadcast_client,
			tls_cert_hash,
			best_effort,
		}
	}

}

impl<C: fcrypto::CSP, S: fcrypto::SignerSerializer> DeliverClient for OrdererDeliverClient<'_, C, S> {

	// GetSpecifiedBlock gets the specified block from a peer/orderer's deliver service
	fn get_specified_block(&self, num: u64) -> Result<common::Block> {
		let envelope = seek_specified(self.csp, self.signer, self.channel_id, num, self.tls_cert_hash.clone(), self.best_effort)?;
		
		let deliver_response = executor::block_on(orderer_deliver(&self.broadcast_client, envelope))?;
		read_orderer_block(deliver_response)
	}

	fn get_oldest_block(&self) -> Result<common::Block> {
		unimplemented!();
	}
	
	fn get_newest_block(&self) -> Result<common::Block> {
		unimplemented!();
	}

	fn close(&self) -> Result<()> {
		unimplemented!();
	}

}

fn seek_specified(csp: &impl fcrypto::CSP, signer: &impl fcrypto::SignerSerializer, channel_id: &str, block_number: u64, tls_cert_hash: Option<Vec<u8>>, best_effort: bool) -> Result<common::Envelope> {
	let mut position = ab::SeekSpecified::new();
	position.set_number(block_number);

	let mut seek_position = ab::SeekPosition::new();
	seek_position.set_specified(position);

	seek_helper(csp, channel_id.to_string(), seek_position, tls_cert_hash, signer, best_effort)
}

fn read_peer_block(mut deliver_response: peer::DeliverResponse) -> Result<common::Block> {
	// let status = deliver_response.get_status();

	// if status != common::Status::SUCCESS {
	// 	error!("Expect status to be SUCCESS, got: {:?}", status);
	// 	let message = format!("Expect status to be SUCCESS, got: {:?}", status);
	// 	return Err(errors::new_fabric_error(message));
	// }

	if !deliver_response.has_block() {
		let messsage = format!("No block in response. Status: {:?}", deliver_response.get_status());
		error!("{}", messsage);
		return Err(errors::new_fabric_error(messsage));
	}

	let block = deliver_response.take_block();

	info!("Received block {}", block.get_header().get_number());

	Ok(block)
}

fn read_orderer_block(mut deliver_response: ab::DeliverResponse) -> Result<common::Block> {
	// let status = deliver_response.get_status();

	// if status != common::Status::SUCCESS {
	// 	error!("Expect status to be SUCCESS, got: {:?}", status);
	// 	let message = format!("Expect status to be SUCCESS, got: {:?}", status);
	// 	return Err(errors::new_fabric_error(message));
	// }

	if !deliver_response.has_block() {
		let messsage = format!("No block in response. Status: {:?}", deliver_response.get_status());
		error!("{}", messsage);
		return Err(errors::new_fabric_error(messsage));
	}

	let block = deliver_response.take_block();

	info!("Received block {}", block.get_header().get_number());

	Ok(block)
}

fn seek_helper(
	csp: &impl fcrypto::CSP,
	channel_id: String,
	position: ab::SeekPosition,
	tls_cert_hash: Option<Vec<u8>>,
	signer: &impl fcrypto::SignerSerializer,
	best_effort: bool,
) -> Result<common::Envelope> {
	let mut seek_info = ab::SeekInfo::new();
	seek_info.set_start(position.clone());
	seek_info.set_stop(position);
	seek_info.set_behavior(ab::SeekInfo_SeekBehavior::BLOCK_UNTIL_READY);

	if best_effort {
		seek_info.set_error_response(ab::SeekInfo_SeekErrorResponse::BEST_EFFORT);
	}

	proto_util::create_signed_envelope_with_tls_binding(
		csp,
		common::HeaderType::DELIVER_SEEK_INFO,
		channel_id,
		Some(signer),
		&seek_info,
		0 as i32,
		0 as u64,
		tls_cert_hash,
	)
}

fn convert_deliver_response(mut response: peer::DeliverResponse) -> Result<ab::DeliverResponse> {
	if response.has_status() {
		let mut ab_resp = ab::DeliverResponse::new();
		let status = response.get_status();
		ab_resp.set_status(status);
		Ok(ab_resp)
	} else if response.has_block() {
		let mut ab_resp = ab::DeliverResponse::new();
		let block = response.take_block();
		ab_resp.set_block(block);
		Ok(ab_resp)
	} else {
		Err(errors::new_fabric_error(format!("response error: unknown type {:?}", response.Type)))
	}
}

async fn peer_deliver_filtered(client: &peer::DeliverClient, envelope: common::Envelope) -> Result<()> {
	let (mut request, response) = client.deliver_filtered(grpc::RequestOptions::new()).await?;

	executor::block_on(async {
		request.wait().await.unwrap();
		request.send_data(envelope)
			.expect("Send data");
		
		request.finish()
			.expect("Finish");
	});

	let mut responses = response.drop_metadata();

	while let Some(response) = responses.next().await {
		let response = response?;
		println!("{:?}", response);
	}
   
	return Ok(());
}

async fn peer_deliver(client: &peer::DeliverClient, envelope: common::Envelope) -> Result<peer::DeliverResponse> {
    let (mut request, response) = client.deliver(grpc::RequestOptions::new()).await?;

    executor::block_on(async {
        request.wait().await.unwrap();
        request.send_data(envelope)
            .expect("Send data");
        
        request.finish()
            .expect("Finish");
    });

    let mut responses = response.drop_metadata();

	if let Some(response) = responses.next().await {
		response.map_err(|err| err.into())
	} else {
		Err(errors::new_fabric_error_str("no response"))
	}
}

async fn orderer_deliver(client: &orderer::AtomicBroadcastClient, envelope: common::Envelope) -> Result<ab::DeliverResponse> {
    let (mut request, response) = client.deliver(grpc::RequestOptions::new()).await?;

    request.wait().await.unwrap();    
    request.send_data(envelope)?;
    request.finish()?;

    let mut responses = response.drop_metadata();

	if let Some(response) = responses.next().await {
		response.map_err(|err| err.into())
	} else {
		Err(errors::new_fabric_error_str("no response"))
	}
}

// struct DeliverClient<'a> {
//     client: &'a peer::DeliverClient,
//     // connection: peer::DeliverClient,
//     address: &'a String,
// }

pub struct DeliverGroup<'a, C: fcrypto::CSP, S: fcrypto::SignerSerializer> {
	csp: &'a C,
	// deliver_clients: Vec<&'a peer::DeliverClient>,
	// deliver_clients: Vec<DeliverClient<'a>>,
	// peer_addresses: Vec<&'a String>,
	signer: &'a S,
	channel_id: &'a str,
	// tx_id: &'a str,
	senders: Vec<thread::JoinHandle<()>>,
}

impl<C: fcrypto::CSP, S: fcrypto::SignerSerializer> DeliverGroup<'_, C, S> {

	pub fn new<'a>(csp: &'a C,
			_peer_addresses: Vec<&'a String>,
			signer: &'a S,
			channel_id: &'a str,
			_tx_id: &'a str) -> DeliverGroup<'a, C, S>{
		// let mut clients = Vec::with_capacity(deliver_clients.len());

		// for (i, deliver_client) in deliver_clients.iter().enumerate() {
		//     let dc = DeliverClient{
		//         client: deliver_client,
		//         address: peer_addresses[i],
		//     };

		//     clients.push(dc);
		// }

		DeliverGroup{
			csp,
			// deliver_clients,
			// peer_addresses,
			signer,
			// certificate,
			channel_id,
			// tx_id,
			senders: Vec::new(),
		}
	}

	/// connect waits for all deliver clients in the group to connect to
	/// the peer's deliver service, receive an error, or for the context
	/// to timeout. An error will be returned whenever even a single
	/// deliver client fails to connect to its peer
	pub fn connect(&mut self, deliver_clients: Vec<peer::DeliverClient>, certificate: Option<tls_api::Certificate>) -> Result<()> {
		let envelope = proto_util::create_deliver_envelope(self.csp, self.signer, self.channel_id.to_string(), certificate)?;
		self.senders = clients_connect(deliver_clients, envelope)?;

		return Ok(());
	}

	// async fn client_connect(&self, deliver_client: DeliverClient<'_>) -> Result<()> {
		// defer dg.wg.Done()

		// if err != nil {
		//     err = errors.WithMessagef(err, "error connecting to deliver filtered at %s", dc.Address)
		//     dg.setError(err)
		//     return
		// }
		// defer df.CloseSend()
		// dc.Connection = df

		// let envelope = create_deliver_envelope(self.channel_id, self.certificate, self.signer)?;
		// err = df.send(envelope)
		// if err != nil {
		//     err = errors.WithMessagef(err, "error sending deliver seek info envelope to %s", dc.Address)
		//     dg.setError(err)
		//     return
		// }

	//     unimplemented!();
	// }

	pub fn wait(&mut self) -> Result<()> {
		for sender in &mut self.senders.drain(..) {
			sender.join().expect("sender thread");        
		}
	   
		return Ok(());
	 }

	// pub fn client_wait(&mut self, deliver_client: &peer::DeliverClient) -> Result<()> {
	//     // defer dg.wg.Done()
		// for {
		//     resp, err := dc.Connection.Recv()
		//     if err != nil {
		//         err = errors.WithMessagef(err, "error receiving from deliver filtered at %s", dc.Address)
		//         dg.setError(err)
		//         return
		//     }
		//     switch r := resp.Type.(type) {
		//     case *pb.DeliverResponse_FilteredBlock:
		//         filteredTransactions := r.FilteredBlock.FilteredTransactions
		//         for _, tx := range filteredTransactions {
		//             if tx.Txid == dg.TxID {
		//                 logger.Infof("txid [%s] committed with status (%s) at %s", dg.TxID, tx.TxValidationCode, dc.Address)
		//                 if tx.TxValidationCode != pb.TxValidationCode_VALID {
		//                     err = errors.Errorf("transaction invalidated with status (%s)", tx.TxValidationCode)
		//                     dg.setError(err)
		//                 }
		//                 return
		//             }
		//         }
		//     case *pb.DeliverResponse_Status:
		//         err = errors.Errorf("deliver completed with status (%s) before txid received", r.Status)
		//         dg.setError(err)
		//         return
		//     default:
		//         err = errors.Errorf("received unexpected response type (%T) from %s", r, dc.Address)
		//         dg.setError(err)
		//         return
		//     }
		// }
		// unimplemented!();
	// }
}

fn clients_connect(clients: Vec<peer::DeliverClient>, envelope: common::Envelope) -> Result<Vec<thread::JoinHandle<()>>> {
	let mut senders = Vec::with_capacity(clients.len());

	for client in clients {
		let envelope_arg = envelope.clone();

		let sender = thread::spawn(move || {
			executor::block_on(peer_deliver_filtered(&client, envelope_arg)).expect("deliver filtered");
		});

		senders.push(sender);
	}
   
	return Ok(senders);
}

pub struct BroadcastClient {
	client: orderer::AtomicBroadcastClient,
}

impl BroadcastClient {
	
	pub fn new(client: orderer::AtomicBroadcastClient) -> BroadcastClient {
		BroadcastClient{
			client,
		}
	}
 
	pub fn send(&self, envelope: common::Envelope) -> Result<()> {
		executor::block_on(send_envelope(&self.client, envelope))

		// err := s.getAck()
	}
	
	pub fn close(&mut self) -> Result<()> {
		// s.Client.CloseSend()

		// unimplemented!();
		Ok(())
	}
}

// pub struct GRPCDeliverClient{
// 	client: orderer::AtomicBroadcastClient,
// }

// impl GRPCDeliverClient {
	
// 	pub fn new(client: orderer::AtomicBroadcastClient) -> GRPCDeliverClient {
// 		GRPCDeliverClient{
// 			client,
// 		}
// 	}
// }

async fn send_envelope(client: &orderer::AtomicBroadcastClient, envelope: common::Envelope) -> Result<()> {
	let (mut request, response) = client.broadcast(grpc::RequestOptions::new()).await?;

	let sender_thread = thread::spawn(move || {
		executor::block_on(async {
			request.wait().await.unwrap();
			request.send_data(envelope).expect("send");
			
			request.finish().expect("finish");
		});
	});

	let mut responses = response.drop_metadata();

	while let Some(response) = responses.next().await {
		let response = response?;
		
		println!("{:?}", response);
	}

	sender_thread.join().expect("sender thread");
	return Ok(());
}
