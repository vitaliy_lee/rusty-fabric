use rusty_fabric::config::{Chaincode, Channel, Peer, Orderer};
use rusty_fabric::service::BroadcastClient;
use rusty_fabric::{OrdererClient, DeliverClient, create_endorsers};

trait Command {
	fn execute(&self) -> Result<()>;
}

struct ChannelCreateCommand {
	channel: 	&Channel,
	orderers: 	Vec<&Orderer>,
}

impl Command for ChannelJoinCommand {

	pub fn execute(&self) -> Result<()> {
		create_channel(self.csp, self.creator_identity, orderer.broadcast_client, orderer.deliver_client, self.channel)
	}

}

struct ChannelJoinCommand {
	channel: 	&Channel,
	peers: 		Vec<&Peer>,
}

impl Command for ChannelJoinCommand {

	pub fn execute(&self) -> Result<()> {
		for peer in org.peers {
			join_channel(self.csp, self.identity, channel.genesis_block, peer.endorser_client)
		}
	}
}

struct ChaincodeInstallCommand {
	package: 	&ChaincodePackage,
	peers: 		Vec<&Peer>,
}

impl Command for ChaincodeInstallCommand {

	pub fn execute(&self) -> Result<()> {
		for peer in peers {
			install_chaincode(self.csp, self.identity, package.path, peer.endorser_client)
		}
	}

}

struct ChaincodeApproveCommand {
	package: 	&ChaincodePackage,
	channel: 	&Channel,
	organizations: Vec<&Organization>,
}

impl Command for ChaincodeApproveCommand {

	pub fn execute(&self) -> Result<()> {
		for org in self.organizations {
			approve_chaincode(self.csp, org.admin_identity, self.channel, self.package, self.options, orderer.broadcast_client, endorser_clients, deliver_clients, None)
		}
	}
}

struct ChaincodeCommitCommand {
	package: 	&ChaincodePackage,
	channel: 	&Channel,
}

impl Command for ChaincodeCommitCommand {

	pub fn execute(&self) -> Result<()> {
		commit_chaincode(self.csp, self.creator_identity, self.channel, self.package, self.options, orderer.broadcast_client, endorser_clients, deliver_clients, None)
	}

}

fn test_commands() {
	let mut commands = Vec::new();

	let command = ChannelCreateCommand {
		channel,
		orderers,
	};

	command.push(command);

	for command in commands {
		command.execute()
			.expect("Execute command");
	}
}

fn test_channel(
	csp: 				&impl fcrypto::CSP,
	channel: 			&Channel,
	chaincode: 			&Chaincode,
	orderers: 			&Orderer,
	peers: 				Vec<&Peer>,
	organizations:		Vec<&Organization>,
) {
	// broadcast_client: 	&impl BroadcastClient,
	// endorser_clients:	Vec<&peer::EndorserClient>,
	// deliver_clients:	Vec<&peer::DeliverClient>,	

	let creator_identity = organizations.nth[0].admin.identity;

	create_channel(csp, creator_identity, orderer.broadcast_client, orderer.deliver_client, channel)
    	.expect("Create channel");

	update_channel(csp, creator_identity, orderer.broadcast_client, channel)
    	.expect("Update channel");

	let package = package_chaincode(chaincode)
		.expect("Package chaincode");

	for org in organizations {
		for peer in org.peers {
			join_channel(csp, identity, channel.genesis_block, peer.endorser_client)
				.expect("Join channel");

			install_chaincode(csp, identity, package.path, peer.endorser_client)
				.expect("Install chaincode");
		}

		// --init-required --sequence 1
		approve_chaincode(csp, org.admin_identity, &chaincode, &channel.id, &package.id, &options, orderer.broadcast_client, endorser_clients, deliver_clients, None)
			.expect("Approve chaincode");
	}

	// once
	commit_chaincode(csp, creator_identity, &chaincode, &channel.id, &options, orderer.broadcast_client, endorser_clients, deliver_clients, None)
		.expect("Commit chaincode");
}

fn test() {
	let channel = Channel {
		name:			"test-channel".to_string(),
		genesis_block: 	"testdata/go_chaincode/fabcar".to_path_buf(),
	};

	let chaincode = Chaincode {
		name: 		"fabcar".to_string(),
		path: 		"testdata/go_chaincode/fabcar".to_path_buf(),
		version: 	"1.0.0".to_string(),
		lang: 		"golang".to_string(),
	};

    let config = rusty_fabric::config::NetworkConfig::read(config_path)
        .expect("Parse config");

    let msp_config = Path::new(&config.identity.msp_config);
    let key_store_path = msp_config.join("keystore");

    // Create CSP
    let csp = crypto::create_oper_csp(&key_store_path);

    // Create Identity
	let identity = crypto::create_identity(&csp, config.identity)
        .expect("Create identity");

    // Create endorsers
    let endorser_clients = create_endorsers(&config.peers.endorsers)
    	.expect("create endorsers");

    let endorser_clients_refs = endorser_clients.iter().collect();

    let orderer_client = OrdererClient::new(config.orderer)
        .expect("create orderer client");

    let broadcast_client = orderer_client.get_broadcast_client()
        .expect("get broadcast client");

	test_channel(&csp, channel, chaincode, orderers, peers, organizations)
        .expect("Test channel");
}